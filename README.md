# Moshi-moshi

Moshi-moshi is an application, developped in Java, to show statistics about a calls platform.

This project use Java [JDK 11.0.2](https://jdk.java.net/archive/) with the external jar [JCalendar 1.4](https://toedter.com/jcalendar/) and [PostgreSQL JDBC Driver 42.2.5](https://jdbc.postgresql.org/).

Of course, Moshi-moshi use [PostgreSQL 9.6](https://www.postgresql.org/).

![](https://framapic.org/u8Y835E7Lt0p/wRwCjZT4FTWv)

# Execute Moshi-moshi
Download the latest artifact named `build-jar-master` [here](https://framagit.org/clement.azibeiro/moshi-moshi/pipelines)

### Require
Make sure you use the 11.0.2 OpenJDK.

## Debian
With the jar :
Execute this command in the `src` directory : `java -jar moshi_moshi.jar`

Else,
`java controllers.Main`

## Windows
Just double click on the `.jar`

## MacOS
With the jar :
Execute this command in the `src` directory : `java -jar moshi_moshi.jar`

# Documentation
[Technical documentation](http://www.bts-malraux72.net/~c.azibeiro/technical_documentation/)