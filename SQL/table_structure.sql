CREATE TABLE "moshi_moshi"."Person" (
    "idPerson" SERIAL NOT NULL,
    "firstname" VARCHAR(20),
    "lastname" VARCHAR(20),

    CONSTRAINT "pk_Person" PRIMARY KEY ("idPerson")
);

CREATE TABLE "moshi_moshi"."ExternPerson"
(
    "phoneNumber" VARCHAR(10),

    CONSTRAINT "pk_ExternPerson" PRIMARY KEY ("idPerson")
)
INHERITS ("moshi_moshi"."Person");

CREATE TABLE "moshi_moshi"."Status"(
    "idStatus" SERIAL NOT NULL,
    "description" VARCHAR(30),

    CONSTRAINT "pk_Status" PRIMARY KEY ("idStatus")
);

CREATE TABLE "moshi_moshi"."Permission"
(
    "idPermission" SERIAL NOT NULL,
    "name" VARCHAR(30),

    CONSTRAINT "pk_Permission" PRIMARY KEY ("idPermission")
);

CREATE TABLE "moshi_moshi"."Role"
(
    "idRole" SERIAL NOT NULL,
    "name" VARCHAR(30),

    CONSTRAINT "pk_Role" PRIMARY KEY ("idRole")
);

CREATE TABLE "moshi_moshi"."RolePermission"
(
    "idRole" INTEGER NOT NULL,
    "idPermission" INTEGER NOT NULL,

    CONSTRAINT "pk_RolePermission" PRIMARY KEY ("idRole", "idPermission"),
    
    CONSTRAINT "fk_Role" FOREIGN KEY ("idRole")
        REFERENCES "moshi_moshi"."Role"("idRole"),
    CONSTRAINT "fk_Permission" FOREIGN KEY ("idPermission")
        REFERENCES "moshi_moshi"."Permission"("idPermission")
);

CREATE TABLE "moshi_moshi"."User"
(
    "login" VARCHAR(30),
    "password" TEXT,
    "idRole" INTEGER NOT NULL,
    "HaveAsSupervisor" INTEGER,
    "IsConnected" BOOLEAN DEFAULT false,

    CONSTRAINT "pk_User" PRIMARY KEY("idPerson"),

    CONSTRAINT "fk_Role" FOREIGN KEY ("idRole")
        REFERENCES "moshi_moshi"."Role"("idRole"),
    CONSTRAINT "fk_User" FOREIGN KEY ("HaveAsSupervisor")
        REFERENCES "moshi_moshi"."User"("idPerson")
)
INHERITS ("moshi_moshi"."Person");

CREATE TYPE direction AS ENUM ('In', 'Out');

CREATE TABLE "moshi_moshi"."Call" (
    "idCall" SERIAL  NOT NULL,
    "idExternPerson" INTEGER NOT NULL,
    "idStatus" INTEGER NOT NULL,
    "idUser" INTEGER NOT NULL,
    "date" date NOT NULL DEFAULT CURRENT_DATE,
    "duration" INTEGER,
    "direction" direction,

    CONSTRAINT "pk_Call" PRIMARY KEY ("idCall"),

    CONSTRAINT "fk_Status" FOREIGN KEY ("idStatus")
        REFERENCES "moshi_moshi"."Status"("idStatus"),
    CONSTRAINT "fk_ExternPerson" FOREIGN KEY ("idExternPerson")
        REFERENCES "moshi_moshi"."ExternPerson"("idPerson"),
    CONSTRAINT "fk_User" FOREIGN KEY ("idUser")
        REFERENCES "moshi_moshi"."User"("idPerson")
);
