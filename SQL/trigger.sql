-- Function: moshi_moshi.hash_update_tg()

-- DROP FUNCTION moshi_moshi.hash_update_tg();

CREATE OR REPLACE FUNCTION moshi_moshi.hash_update_tg()
  RETURNS trigger AS
$BODY$
BEGIN
    IF tg_op = 'INSERT' THEN
        NEW."password" = encode(moshi_moshi.digest(NEW."password", 'sha512'), 'hex');
        RETURN NEW;
    END IF;
END;
$BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100;
  
-- Trigger: User on moshi_moshi."User"

-- DROP TRIGGER "User" ON moshi_moshi."User";

CREATE TRIGGER "User"
  BEFORE INSERT
  ON moshi_moshi."User"
  FOR EACH ROW
  EXECUTE PROCEDURE moshi_moshi.hash_update_tg();