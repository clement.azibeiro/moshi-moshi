$pdflatex = 'pdflatex --shell-escape %O %S';
$recorder = 1;

$hash_calc_ignore_pattern{'pdf'} = '.*';

add_cus_dep('puml', 'pdf', 0, 'plantuml');
sub plantuml {
    system("plantuml -tpdf '$_[0]'.puml");
    &cus_dep_require_primary_run;
}