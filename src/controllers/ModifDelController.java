package controllers;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.List;

import encryption.Encryption;
import model.DAO.DAO;
import model.DAO.RoleDAO;
import model.DAO.UserDAO;
import model.classes.Role;
import model.classes.User;
import views.ModifDelView;

/**
 * ModifDelController is the class which manage the event from the view to edit or delete a user
 * 
 * @author Clement AZIBEIRO
 * @version 1.0
 */
public class ModifDelController implements ActionListener {
	
	private ModifDelView modifDelView;
	private boolean btnEditPressed, btnDelPressed;
	User user_connected;
	private DAO<User> user_dao = new UserDAO();
	private List<User> user_list = user_dao.recupAll();
	private String password = "**********";;
	
	/**
	 * Constructor of the AddUserController class
	 * @param modifDelView The view to edit or delete a user
	 * @param btnEditPressed The status of the button edit (if it pressed) 
	 * @param btnDelPressed The status of the button delete (if it pressed) 
	 */
	public ModifDelController(ModifDelView modifDelView, User user_connected,boolean btnEditPressed, boolean btnDelPressed)
	{
		this.modifDelView = modifDelView;
		this.btnEditPressed = btnEditPressed;
		this.btnDelPressed = btnDelPressed;
		this.user_connected = user_connected;
		
		// Add all user in the combo box ComboBoxUser if the user connected is a supervisor
		if(user_connected.getSupervisor().getFirstname() == null)
		{
			modifDelView.getComboBoxRole().setEnabled(true);
			modifDelView.getComboBoxSupervisor().setEnabled(true);
			
			for(User user : user_list)
			{
				modifDelView.getComboBoxUserSelection().addItem(user.getLogin());
			}
		}
		else
		{
			modifDelView.getComboBoxRole().setEnabled(false);
			modifDelView.getComboBoxSupervisor().setEnabled(false);
			modifDelView.getComboBoxUserSelection().addItem(user_connected.getLogin());
		}
		// Get the selected user and fill the JTextField and combo boxes
		setTextAndCombo(selectedUser());
		
		// Configure the view based on the button pressed
		if (btnEditPressed)
		{
			configureModifUserView();
		}
		else if (btnDelPressed)
		{
			configureDelUserView();
			disableTextAndComboBox();
		}
	}
	
	/**
	 * ActionListener override method used to listening action on JButtons and Combo boxes
	 */
	@Override
	public void actionPerformed(ActionEvent e) {
		if(e.getSource() == modifDelView.getBtnCancel())
		{
			modifDelView.dispose();
			disableTextAndComboBox();
		}
		else if(e.getSource() == modifDelView.getBtnModifDel())
		{
			if(btnDelPressed)
			{
				user_dao.delete(selectedUser());
				modifDelView.dispose();
			}
			else if(btnEditPressed)
			{
				user_dao.update(modifiedUser());
				modifDelView.dispose();
			}
		}
		else if(e.getSource() == modifDelView.getComboBoxUserSelection())
		{
			setTextAndCombo(selectedUser());
		}
	}
	
	/**
	 * Instance a user with the modified data in the edit view
	 * @return An instance of user, modified
	 */
	public User modifiedUser()
	{
		User update_user = null;
		
		if(modifDelView.getComboBoxSupervisor().isEnabled())
		{
			String password_to_update;
			
			if(!String.valueOf(modifDelView.getTxtPassword().getPassword()).equals(password))
			{
				password_to_update = Encryption.encryptThisString(String.valueOf(modifDelView.getTxtPassword().getPassword()));
			}
			else
			{
				password_to_update = selectedUser().getPassword();
			}
			
			update_user = new User(selectedUser().getIdPerson(),
									modifDelView.getTxtFirstname().getText(),
									modifDelView.getTxtLastname().getText(),
									modifDelView.getTxtLogin().getText(),
									password_to_update,
									selectedRole(),
									selectedSupervisor());
		}
		else
		{
			// Instance a user without supervisor
			update_user = new User(selectedUser().getIdPerson(),
					modifDelView.getTxtFirstname().getText(),
					modifDelView.getTxtLastname().getText(),
					modifDelView.getTxtLogin().getText(),
					Encryption.encryptThisString(String.valueOf(modifDelView.getTxtPassword().getPassword())),
					selectedRole(),
					null);
		}
		
		return update_user;
	}
	
	/**
	 * Method used to get the selected supervisor in the combo box
	 * @return An instance of user, the supervisor selected
	 */
	public User selectedSupervisor()
	{
		User selected_supervisor = null;
		
		for(User supervisor : user_list)
		{
			if(modifDelView.getComboBoxSupervisor().getSelectedItem().equals(supervisor.getFirstname() + " " + supervisor.getLastname()))
			{
				selected_supervisor = supervisor;
			}
		}
		
		return selected_supervisor;
	}
	
	/**
	 * Method used to get the selected role in the combo box
	 * @return An instance of role, The role selected
	 */
	public Role selectedRole()
	{
		DAO<Role> role_dao = new RoleDAO();
		List<Role> role_list = role_dao.recupAll();
		
		Role user_role = null;
		
		for(Role role : role_list)
		{
			if(modifDelView.getComboBoxRole().getSelectedItem().toString().contains(Integer.toString(role.getIdRole())))
			{
				user_role = role;
			}
		}
		
		return user_role;
	}
	
	/**
	 * Method used to get the selected user in the combo box
	 * @return An instance of user, the user selected
	 */
	public User selectedUser()
	{
		User user_selected = null;
		
		for(User user : user_list)
		{
			if(user.getLogin().equals(modifDelView.getComboBoxUserSelection().getSelectedItem().toString()))
			{
				user_selected = user;
			}
		}
		
		return user_selected;
	}
	
	/**
	 * Fill the JTextField and JComboBox with the data of the user selected
	 * @param user_selected The user selected in the combo box
	 */
	public void setTextAndCombo(User user_selected)
	{
		// Clean the ComboBoxes
		modifDelView.getComboBoxRole().removeAllItems();
		modifDelView.getComboBoxSupervisor().removeAllItems();
		
		// Enabled the combo box of supervisors when it's the view to edit is called and if the user connected is a supervisor
		if(!modifDelView.getComboBoxSupervisor().isEnabled() && btnEditPressed && user_connected.getSupervisor().getFirstname() == null)
		{
			modifDelView.getComboBoxSupervisor().setEnabled(true);
		}
		
		// Setting all the text field with the user data
		modifDelView.getTxtFirstname().setText(user_selected.getFirstname());
		modifDelView.getTxtLastname().setText(user_selected.getLastname());
		modifDelView.getTxtLogin().setText(user_selected.getLogin());
		modifDelView.getTxtPassword().setText(password);
		
		// Establish the connection with the DB and get all the role
		DAO<Role> role_dao = new RoleDAO();
		List<Role> role_list = role_dao.recupAll();
		
		// Add the current role of the user in the combo box
		modifDelView.getComboBoxRole().addItem(user_selected.getUser_role().getIdRole() + " - " + user_selected.getUser_role().getDescription());
		
		// Search the other roles to add in the combo box
		for(Role role : role_list)
		{
			if(!role.getDescription().equals(user_selected.getUser_role().getDescription()))
			{
				modifDelView.getComboBoxRole().addItem(role.getIdRole() + " - "+ role.getDescription());
			}
		}
		
		// Insert the current supervisor of the user in the combo box and add the other
		if(user_selected.getSupervisor().getFirstname() != null)
		{
			String supervisor_user_select = user_selected.getSupervisor().getFirstname() + " " + user_selected.getSupervisor().getLastname();
			
			modifDelView.getComboBoxSupervisor().addItem(supervisor_user_select);
			
			for(User user : user_list)
			{
				if(!modifDelView.getComboBoxSupervisor().getSelectedItem().equals(user.getFirstname() + " " + user.getLastname()) && user.getSupervisor().getFirstname() == null)
				{
					modifDelView.getComboBoxSupervisor().addItem(user.getFirstname() + " " + user.getLastname());
				}
			}
		}
		else
		{
			modifDelView.getComboBoxSupervisor().addItem("");
			modifDelView.getComboBoxSupervisor().setEnabled(false);
		}
	}
	
	/**
	 * Method used to configure the view to edit a user
	 */
	public void configureModifUserView()
	{
		modifDelView.getLblModifDel().setText("User modification :");
		modifDelView.getLblSure().setText("Are you sure to update this user ?");
		modifDelView.getBtnModifDel().setText("Update");
	}
	
	// Method used to configure the view to delete a user
	public void configureDelUserView()
	{
		modifDelView.getLblModifDel().setText("Delete user :");
		modifDelView.getLblSure().setText("Are you sure to delete this user ?");
		modifDelView.getBtnModifDel().setText("Delete");
	}
	
	/**
	 * Method used when the view to delete a user is called, this disable all the texts fields and combo boxes
	 */
	public void disableTextAndComboBox()
	{
		modifDelView.getTxtFirstname().setEditable(false);
		modifDelView.getTxtLastname().setEditable(false);
		modifDelView.getTxtLogin().setEditable(false);
		modifDelView.getTxtPassword().setEditable(false);
		modifDelView.getComboBoxRole().setEnabled(false);
		modifDelView.getComboBoxSupervisor().setEnabled(false);
	}
}
