package controllers;

import java.awt.Color;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.sql.Date;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.List;
import java.util.Locale;

import javax.swing.JTable;
import javax.swing.Timer;
import javax.swing.table.DefaultTableCellRenderer;
import javax.swing.table.DefaultTableModel;
import javax.swing.table.TableColumn;

import model.DAO.CallDAO;
import model.DAO.DAO;
import model.DAO.RolePermissionDAO;
import model.DAO.UserDAO;
import model.classes.Call;
import model.classes.RolePermission;
import model.classes.User;
import table.TableColorRenderer;
import views.AddUserView;
import views.LoginView;
import views.MainView;
import views.ModifDelView;

/**
 * MainController is the class which manage the event from the main view
 * 
 * @author Clement AZIBEIRO
 * @version 1.0
 */
public class MainController implements ActionListener, MouseListener {
	
	private MainView mainView;
	private int call_failed, call_success, total_sec, passedOnFillUserTable = 0, passedOnFillCallTable =0;
	private boolean btnCallPressed = false, btnCallTimePressed = false, btnFailedCallPressed = false, btnSuccessCallPressed = false, btnOperatorsPressed = false;
	private User user_connected;
	// Establish all the connection with the DB
	private DAO<Call> call_dao = new CallDAO();
	private DAO<User> user_dao = new UserDAO();
	private DAO<RolePermission> role_permission_dao = new RolePermissionDAO();
	// Get all the data from database to compare this list with the new list if there is update in DB
	private List<User> old_user_list;
	private List<Call> old_today_call_list = ((CallDAO) call_dao).recupAllDate(new java.sql.Date(new java.util.Date().getTime()));;
	private List<Call> old_month_call_list = ((CallDAO) call_dao).recupAllDate("month"); 
	private List<Call> old_year_call_list = ((CallDAO) call_dao).recupAllDate("year");
	
	/**
	 * Constructor of the MainController class
	 * @param mainView The main view
	 * @param user_connected The user connected
	 */
	public MainController(MainView mainView, User user_connected)
	{
		// Put the mainView in the current variable mainView
		this.mainView = mainView;
		this.user_connected = user_connected;
		
		old_user_list = user_dao.recupAll();
		
		// Get the permission "Create" of the connected user
		RolePermission permission_creation_connected_user = ((RolePermissionDAO) role_permission_dao).readWithPermission(Integer.toString(user_connected.getUser_role().getIdRole()), "3");
		// Get the permission "Modification" of the connected user
		RolePermission permission_mofication_connected_user = ((RolePermissionDAO) role_permission_dao).readWithPermission(Integer.toString(user_connected.getUser_role().getIdRole()), "2");
		// Get the permission "Delete" of the connected user
		RolePermission permission_delete_connected_user = ((RolePermissionDAO) role_permission_dao).readWithPermission(Integer.toString(user_connected.getUser_role().getIdRole()), "4");
		
		// disable buttons based on the use permission 
		if(permission_creation_connected_user == null)
		{
			mainView.getBtnAddUser().setEnabled(false);
		}
		if(permission_mofication_connected_user == null)
		{
			mainView.getBtnEditUser().setEnabled(false);
		}
		if(permission_delete_connected_user == null)
		{
			mainView.getBtnDeleteUser().setEnabled(false);
		}

		// Initialize a new timer Listener with a timer
		ActionListener timerListener = new ActionListener()
	    {
			List<Call> new_today_call_list, new_month_call_list, new_year_call_list, date_call_list;
			List<User> new_user_list;
			DateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy");;
			
			// Creating a new ActionEvent who will update every seconds
	        public void actionPerformed(ActionEvent e)
	        {
	        	Date date_now = new java.sql.Date(new java.util.Date().getTime());
    			call_failed = 0;
    			call_success = 0;
    			total_sec = 0;
    			
    			// Get all user
    			new_user_list = user_dao.recupAll();
    			// Get all the call in the actual day
    			new_today_call_list = ((CallDAO) call_dao).recupAllDate(date_now);
    			// Get all the call in the actual mouth
    			new_month_call_list = ((CallDAO) call_dao).recupAllDate("month");
    			// Get all the call in the actual year
    			new_year_call_list = ((CallDAO) call_dao).recupAllDate("year");
	    		
	    		// Set the label for numbers of operators
	    		mainView.getLblOperators().setText(Integer.toString(searchOperator(new_user_list)));
	    		
	    		// Update the table based on the radio button selected
	    		if(mainView.getRdbtnToday().isSelected())
	    		{
	    			// Put the current date and time in the label date and update it
	        		DateFormat heureFormat = new SimpleDateFormat("HH:mm:ss");
		    		mainView.getLblDate().setText("<html>" + dateFormat.format(date_now).toString() + "<br>" +
		    						heureFormat.format(date_now) + "</html>");
		    		
		    		verifyCallButtonPressed(new_today_call_list, old_today_call_list);
		    		verifyOperatorButtonPressed(new_user_list);
		    		calculateCallTimeAndStatus(new_today_call_list);
	    			
		    		// Set the label of the total calls
		    		mainView.getLblTotalCall().setText(Integer.toString(new_today_call_list.size()));
	    		}
	    		else if (mainView.getRdbtnMonth().isSelected())
	    		{
	    			// Put the current MONTH in the label date and update it
	    			mainView.getLblDate().setText(Calendar.getInstance().getDisplayName(Calendar.MONTH, Calendar.LONG, Locale.ENGLISH) + " " 
	    											+ Integer.toString(Calendar.getInstance().get(Calendar.YEAR)));
	    			
	    			verifyCallButtonPressed(new_month_call_list, old_month_call_list);
	    			verifyOperatorButtonPressed(new_user_list);
	    			calculateCallTimeAndStatus(new_month_call_list);
	    			
		    		// Set the label of the total calls
		    		mainView.getLblTotalCall().setText(Integer.toString(new_month_call_list.size()));
	    		}
	    		else if (mainView.getRdbtnThisYear().isSelected())
	    		{
	    			// Put the current YEAR in the label date and update it
	    			mainView.getLblDate().setText(Integer.toString(Calendar.getInstance().get(Calendar.YEAR)));
	    			
	    			verifyCallButtonPressed(new_year_call_list, old_year_call_list);
	    			verifyOperatorButtonPressed(new_user_list);
	    			calculateCallTimeAndStatus(new_year_call_list);
	    			
	    			// Set the label of the total calls
		    		mainView.getLblTotalCall().setText(Integer.toString(new_year_call_list.size()));
	    		}
	    		else if (mainView.getRdbtnDate().isSelected() && mainView.getDateChooser().getCalendar() != null)
	    		{
	    			java.util.Date date_selected = mainView.getDateChooser().getCalendar().getTime();
	    			// Get all the call in the actual day
	    			date_call_list = ((CallDAO) call_dao).recupAllDate(new java.sql.Date(date_selected.getTime()));
	    			
	    			// Put the current date and time in the label date and update it
	        		mainView.getLblDate().setText(dateFormat.format(date_selected).toString());
	        		
	        		verifyCallButtonPressed(date_call_list, date_call_list);
	        		verifyOperatorButtonPressed(new_user_list);
	        		calculateCallTimeAndStatus(date_call_list);
	    			
	        		// Set the label of the total calls
	        		mainView.getLblTotalCall().setText(Integer.toString(date_call_list.size()));
	    		}
	    		else
	    		{
	    			disableDataLabel();
	    		}
	        }
	    };
	    // Instance a new timer with 1000 ms of delay
	    Timer timer = new Timer(1000, timerListener);
	    // to make sure it doesn't wait one second at the start
	    timer.setInitialDelay(0);
	    timer.start();   
	}

	/**
	 * ActionListener override method used to listening action on JButtons and JRadioButton
	 */
	@Override
	public void actionPerformed(ActionEvent e) {
		/** 
		 * Determinate which button is pressed,
		 * as long as the user don't click on the back button, (he stay on the table)
		 * the program consider that the button is pressed.
		 */
		if(e.getSource() == mainView.getBtnTotalCall())
		{
			btnCallPressed = true;
			
			// Setting the label lblTableTitle
			mainView.getLblTableTitle().setText("Total calls");
			mainView.getLblTableTitle().setVisible(true);
		}
		else if(e.getSource() == mainView.getBtnCallTime())
		{
			btnCallTimePressed = true;
		}
		else if(e.getSource() == mainView.getBtnFailedCall())
		{
			btnFailedCallPressed = true;
		}
		else if(e.getSource() == mainView.getBtnSuccessCall())
		{
			btnSuccessCallPressed = true;
		}
		else if(e.getSource() == mainView.getBtnOperators())
		{
			btnOperatorsPressed = true;
		}
		// Verify if the JButton back is pressed and it visible
		else if(e.getSource() == mainView.getBtnBack() && mainView.getBtnBack().isVisible())
		{
			mainView.getScrollPane().setVisible(false);
			mainView.getTable().setVisible(false);
			mainView.getBtnBack().setVisible(false);
			mainView.getLblTableTitle().setVisible(false);
			mainView.getBtnAddUser().setVisible(false);
			mainView.getBtnEditUser().setVisible(false);
			mainView.getBtnDeleteUser().setVisible(false);
			
			enalbeQuitDisconnectButton();
			
			enabledAllLabel();
			
			btnOperatorsPressed = false;
			btnSuccessCallPressed = false;
			btnFailedCallPressed = false;
			btnCallTimePressed = false;
			btnCallPressed = false;

			passedOnFillUserTable = 0;
			passedOnFillCallTable = 0;
		}
		// Test if the button operators is pressed and the button add, edit or delete user too
		else if(btnOperatorsPressed && e.getSource() == mainView.getBtnAddUser())
		{
			// Instance and show the view to add user
			AddUserView add_user_view = new AddUserView();
			add_user_view.setVisible(true);
			
			// Instance the controller
			AddUserController add_user_controller = new AddUserController(add_user_view);
			add_user_view.getBtnCancel().addActionListener(add_user_controller);
			add_user_view.getBtnCreate().addActionListener(add_user_controller);
		}
		else if(btnOperatorsPressed && e.getSource() == mainView.getBtnEditUser())
		{
			ModifDelView modifDelView = new ModifDelView();
			modifDelView.setVisible(true);
			
			ModifDelController modif_del_controller = new ModifDelController(modifDelView, user_connected, true, false);
			modifDelView.getBtnCancel().addActionListener(modif_del_controller);
			modifDelView.getBtnModifDel().addActionListener(modif_del_controller);
			modifDelView.getComboBoxUserSelection().addActionListener(modif_del_controller);
		}
		else if(btnOperatorsPressed && e.getSource() == mainView.getBtnDeleteUser())
		{
			ModifDelView modifDelView = new ModifDelView();
			modifDelView.setVisible(true);
			
			ModifDelController modif_del_controller = new ModifDelController(modifDelView, user_connected, false, true);
			modifDelView.getBtnCancel().addActionListener(modif_del_controller);
			modifDelView.getBtnModifDel().addActionListener(modif_del_controller);
			modifDelView.getComboBoxUserSelection().addActionListener(modif_del_controller);
		}
		// Those tests are used to reload the table when the user change radio button
		else if(e.getSource() == mainView.getRdbtnToday())
		{
			passedOnFillCallTable = 0;
		}
		else if(e.getSource() == mainView.getRdbtnMonth())
		{
			passedOnFillCallTable = 0;
		}
		else if(e.getSource() == mainView.getRdbtnThisYear())
		{
			passedOnFillCallTable = 0;
		}
		else if(e.getSource() == mainView.getRdbtnDate())
		{
			passedOnFillCallTable = 0;
		}
		// Disconnect the user if the button disconnect is pressed
		else if(e.getSource() == mainView.getBtnDisconnect())
		{
			mainView.dispose();
			LoginView loginView = new LoginView();
			loginView.setVisible(true);
			LoginController loginController = new LoginController(loginView);
			loginView.getBtnLogin().addActionListener(loginController);
		}
		// Quit the program if the button quit is pressed
		else if(e.getSource() == mainView.getBtnQuit())
		{
			mainView.dispose();
			System.exit(0);
		}
	}
	
	/**
	 * MouseListener override method used to listening action on cell in the JTable
	 */
	@Override
	public void mouseClicked(MouseEvent e) {
		int row = mainView.getTable().rowAtPoint(e.getPoint());
	    int col = mainView.getTable().columnAtPoint(e.getPoint());
	    List<User> user_list = user_dao.recupAll();
	    RolePermission role_permission = null;
	    User user_status_update = null;
	    Boolean status = false;   
	    
	    role_permission = ((RolePermissionDAO) role_permission_dao).readWithPermission(Integer.toString(user_connected.getUser_role().getIdRole()), "2");
	    
	    // If the user have the permission to modify
	    if(role_permission != null)
	    {
		    // Test if the column status is clicked
		    if (col == 5) 
		    {
		    	// get the value of the operator selected
		    	String firstname = mainView.getTable().getValueAt(row, 0).toString();
		 	    String lastname = mainView.getTable().getValueAt(row, 1).toString();
		 	    String login = mainView.getTable().getValueAt(row, 2).toString();
		 	    
		 	    // get the user in the list
		 	    for(User user : user_list)
		 	    {  
		 	    	if(user.getFirstname().equals(firstname)
		 	    		&& user.getLastname().equals(lastname)
		 	    		&& user.getLogin().equals(login))
		 	    	{
		 	    		user_status_update = user;
		 	    	}
		 	    }
		 	    
		 	    // Only the user connected can change is status or a supervisor
		 	    if((user_connected.getLogin().equals(user_status_update.getLogin())
		 	    	&& user_connected.getPassword().equals(user_status_update.getPassword()))
		 	    	|| user_connected.getSupervisor().getFirstname() == null)
		 	    {
			 	    // Update the status in the table
			    	if(user_status_update.getIsConnected())
			    	{
			    		mainView.getTable().setValueAt(false, row, col);
			    		status = false;
			    	}
			    	else if (!user_status_update.getIsConnected())
			    	{
			    		mainView.getTable().setValueAt(true, row, col);
			    		status = true;
			    	}
			    	
			    
			    
				    // Update the status in the DB
				    if(user_status_update != null )
				    {
					    user_status_update.setIsConnected(status);
				        user_dao.update(user_status_update);
				    }
		 	    }
		    }
	    }
	}
	
	/* #######################################################################################################
	 * Functions used to verify if the buttons are pressed
	 * #######################################################################################################
	 */
	
	/**
	 * Verify if any button that will show a call table is pressed
	 * @param old_call_list A old call list (not update every second)
	 * @param new_call_list A new call list (update every second with the DB)
	 */
	public void verifyCallButtonPressed(List<Call> new_call_list, List<Call> old_call_list)
	{
		// Verify if the JButton Total Call or Call time is pressed
		if((btnCallPressed || btnCallTimePressed))
	    {
			disableQuitDisconnectButton();
			
			// Clean the table
			mainView.getTable().setDefaultRenderer(Object.class, new DefaultTableCellRenderer());
			mainView.getTable().removeAll();	
			
			// Setting the label lblTableTitle
			mainView.getLblTableTitle().setText("Total calls");
			mainView.getLblTableTitle().setVisible(true);
			
			testCallList(old_call_list, new_call_list);
	    }
		else if(btnFailedCallPressed)
		{
			disableQuitDisconnectButton();
			
			// Clean the table
			mainView.getTable().setDefaultRenderer(Object.class, new DefaultTableCellRenderer());
			mainView.getTable().removeAll();
			
			// Setting the label lblTableTitle
			mainView.getLblTableTitle().setText("Failed calls");
			mainView.getLblTableTitle().setVisible(true);
			
			testCallList(old_call_list, new_call_list);
			
			// Color the column Status in red
			TableColumn column = mainView.getTable().getColumnModel().getColumn(5);
			DefaultTableCellRenderer renderer = new DefaultTableCellRenderer();
			renderer.setForeground(Color.RED);
			column.setCellRenderer(renderer);
			
		}
		else if(btnSuccessCallPressed)
		{
			disableQuitDisconnectButton();
			
			// Clean the table
			mainView.getTable().setDefaultRenderer(Object.class, new DefaultTableCellRenderer());
			mainView.getTable().removeAll();

			// Setting the label lblTableTitle
			mainView.getLblTableTitle().setText("Successful calls");
			mainView.getLblTableTitle().setVisible(true);
			
			testCallList(old_call_list, new_call_list);
			
			// Color the column Status in green
			TableColumn column = mainView.getTable().getColumnModel().getColumn(5);
			DefaultTableCellRenderer renderer = new DefaultTableCellRenderer();
			renderer.setForeground(Color.GREEN);
			column.setCellRenderer(renderer);
		}
		
	}
	
	/**
	 * Verify if the button operators is pressed
	 * @param new_user_list A user list
	 */
	public void verifyOperatorButtonPressed(List<User> new_user_list)
	{
		if(btnOperatorsPressed)
		{
			disableQuitDisconnectButton();
			mainView.getTable().removeAll();

			// Setting the label lblTableTitle
			mainView.getLblTableTitle().setText("Operators");
			mainView.getLblTableTitle().setVisible(true);
			mainView.getBtnAddUser().setVisible(true);
			mainView.getBtnEditUser().setVisible(true);
			mainView.getBtnDeleteUser().setVisible(true);
			
			// Compare the list and reload the JTable if it change
			if(compareTwoUserList(old_user_list, new_user_list) && passedOnFillUserTable == 0)
			{	
				fillUserTable(old_user_list);
				passedOnFillUserTable++;
			}
			else if(!compareTwoUserList(old_user_list, new_user_list))
			{
				fillUserTable(new_user_list);
				passedOnFillUserTable = 0;
			}
			
			// Update the table with a color status, RED for disconnected and GREEN for connected
			mainView.getTable().setDefaultRenderer(Object.class, new DefaultTableCellRenderer());
			TableColorRenderer renderer = new TableColorRenderer();
			mainView.getTable().setDefaultRenderer(Object.class, renderer);
		}
	}
	
	/* #######################################################################################################
	 * Functions used to create and fill call table
	 * #######################################################################################################
	 */
	
	/**
	 * Test if the old call list is up to date
	 * @param old_call_list A old call list (not update every second)
	 * @param new_call_list A new call list (update every second with the DB)
	 */
	public void testCallList(List<Call> old_call_list, List<Call> new_call_list)
	{
		if(compareTwoCallList(old_call_list, new_call_list) && passedOnFillCallTable == 0)
		{	
			fillCallTable(old_call_list);
			passedOnFillCallTable++;
		}
		else if(!compareTwoCallList(old_call_list, new_call_list))
		{
			fillCallTable(new_call_list);
			old_call_list = new_call_list;
			passedOnFillCallTable = 0;
		}
	}
	
	/**
	 * Fill the data in the call table
	 * @param data Double dimensional Object array of the data
	 * @param call_list A call list to fill in the table
	 * @param index Index of the loop
	 */
	public void fillDataCallTable(Object[][] data, List<Call> call_list, int index)
	{
		String direction = "";
		
		// Set the direction of the call
		if(call_list.get(index).getDirection().toString().equals("Out"))
		{
			direction = "Outgoing";
		}
		else
		{
			direction = "Incomming";
		}
		
		// set the data we will insert in the table
		data[index][0] = call_list.get(index).getDate();
		data[index][1] = convertSecToMin(call_list.get(index).getDuration());
		data[index][2] = direction;
		data[index][3] = call_list.get(index).getExtern_person().getPhoneNumber();
		data[index][4] = call_list.get(index).getOperator().getLogin();
		data[index][5] = call_list.get(index).getOne_status().getDescription();
	}
	
	/**
	 * Fill the call table
	 * @param call_list A call list to fill in the table
	 */
	public void fillCallTable(List<Call> call_list)
	{
		// Set the header and the length if the JTable
		Object[] headers = {"Date", "Duration", "Direction", "Phonenumber","Operator", "Status"};
		Object[][] data = new Object[call_list.size()][headers.length];
		
		// Search which button is pressed and fill the Jtable
		for(int i =0; i < call_list.size(); i++)
		{
			if(call_list.get(i).getOne_status().getIdStatus() != 1 && btnFailedCallPressed )
			{
				fillDataCallTable(data, call_list, i);
			}
			else if(call_list.get(i).getOne_status().getIdStatus() == 1 && btnSuccessCallPressed)
			{
				fillDataCallTable(data, call_list, i);
			}
			else if (btnSuccessCallPressed == false && btnFailedCallPressed == false)
			{
				fillDataCallTable(data, call_list, i);
			}
		}
		
		// Create the Table and show it
		createTable(headers, data);
		
		// Destroy array and list
		headers = null;
		data = null;
	}
	
	/* #######################################################################################################
	 * Functions used to create and fill user table
	 * #######################################################################################################
	 */
	
	/**
	 * Fill the user table 
	 * @param user_list A user list to fill in the table
	 */
	public void fillUserTable(List<User> user_list)
	{
		// Set the header and the length if the JTable
		Object[] headers = {"Firstname", "Lastname", "Login", "Role", "Supervisor", "Status"};
		Object[][] data = new Object[user_list.size()][headers.length];

		fillDataUserTable(data, user_list);
		
		createTable(headers, data);
	}
	
	
	/**
	 * Fill the data in the user table
	 * @param data Double dimensional Object array of the data
	 * @param user_list A user list to fill in the table
	 */
	public void fillDataUserTable(Object[][] data, List<User> user_list)
	{
		for(int index = 0; index < user_list.size(); index++)
		{
			// fill the table with only operators
			if(!(user_list.get(index).getSupervisor().getFirstname() == null))
			{
				data[index][0] = user_list.get(index).getFirstname();
				data[index][1] = user_list.get(index).getLastname();
				data[index][2] = user_list.get(index).getLogin();
				data[index][3] = user_list.get(index).getUser_role().getDescription();
				data[index][4] = user_list.get(index).getSupervisor().getFirstname() + " " + user_list.get(index).getSupervisor().getLastname();
				data[index][5] = user_list.get(index).getIsConnected(); // This cell will have a color
			}
		}

	}
	
	/**
	 * Create the JTable in the main views and show it
	 * @param headers Headers of the table
	 * @param data Data of the table
	 */
	public void createTable(Object[] headers, Object[][] data)
	{
		// Instance a new model of table with header and data
		DefaultTableModel defaultTableModel = new DefaultTableModel(data, headers);
		
		// Delete empty rows
		for (int i = defaultTableModel.getRowCount() - 1; i >= 0; i--)
		{
		    if (defaultTableModel.getValueAt(i, 0) == null)
		    	defaultTableModel.removeRow(i);
		}
		
		disableAllLabel();
		mainView.getBtnBack().setVisible(true);
		
		// Setting the model and the visibility of the table 
		mainView.getTable().setModel(defaultTableModel);
		mainView.getTable().setAutoResizeMode(JTable.AUTO_RESIZE_ALL_COLUMNS);
		mainView.getTable().setVisible(true);
		mainView.getScrollPane().setVisible(true);
	}
	
	/* #######################################################################################################
	 * Functions used to manipulate the views
	 * #######################################################################################################
	 */
	
	/**
	 *  Disable all the label, used when the user click on a button
	 */
	public void disableAllLabel()
	{
		mainView.getBtnCallTime().setVisible(false);
		mainView.getBtnFailedCall().setVisible(false);
		mainView.getBtnOperators().setVisible(false);
		mainView.getBtnSuccessCall().setVisible(false);
		mainView.getBtnTotalCall().setVisible(false);
		
		mainView.getLblCallTime().setVisible(false);
		mainView.getLblDate().setVisible(false);
		mainView.getLblFailedCall().setVisible(false);
		mainView.getLblOperators().setVisible(false);
		mainView.getLblSuccessCall().setVisible(false);
		mainView.getLblTotalCall().setVisible(false);
	}
	
	/**
	 * Disable the label where data is insert, used when the radio button of a specific date is selected without date
	 */
	public void disableDataLabel()
	{
		mainView.getLblCallTime().setVisible(false);;
		mainView.getLblTotalCall().setVisible(false);
		mainView.getLblDate().setText("");
		mainView.getLblFailedCall().setVisible(false);
		mainView.getLblSuccessCall().setVisible(false);
	}
	
	/**
	 * Enable all the label, used when the user click the button back to return on main view
	 */
	public void enabledAllLabel()
	{
		mainView.getBtnCallTime().setVisible(true);
		mainView.getBtnFailedCall().setVisible(true);
		mainView.getBtnOperators().setVisible(true);
		mainView.getBtnSuccessCall().setVisible(true);
		mainView.getBtnTotalCall().setVisible(true);
		
		mainView.getLblCallTime().setVisible(true);
		mainView.getLblDate().setVisible(true);
		mainView.getLblFailedCall().setVisible(true);
		mainView.getLblOperators().setVisible(true);
		mainView.getLblSuccessCall().setVisible(true);
		mainView.getLblTotalCall().setVisible(true);
	}
	
	/**
	 * Enable the label where data is insert, used when the radio button and a specific date is selected
	 */
	public void enabledDataLabel()
	{
		mainView.getLblCallTime().setVisible(true);;
		mainView.getLblTotalCall().setVisible(true);
		mainView.getLblFailedCall().setVisible(true);
		mainView.getLblSuccessCall().setVisible(true);
	}
	
	/**
	 * Disable buttons Quit and Disconnect, used when the user pressed any button to show table
	 */
	public void disableQuitDisconnectButton()
	{
		mainView.getBtnDisconnect().setVisible(false);
		mainView.getBtnQuit().setVisible(false);
	}
	
	/**
	 * Show buttons Quit and Disconnect, used when the user go back to main view
	 */
	public void enalbeQuitDisconnectButton()
	{
		mainView.getBtnDisconnect().setVisible(true);
		mainView.getBtnQuit().setVisible(true);
	}
	
	/* #######################################################################################################
	 * Functions used to compute
	 * #######################################################################################################
	 */
	
	/**
	 * Search all operators in a list
	 * @param user_list A user list
	 * @return Integer the number of operator in the user list
	 */
	public int searchOperator(List<User> user_list)
	{
		// Search the number of operator
		int operator = 0;
		for(User user : user_list)
		{
			if(!(user.getSupervisor().getFirstname() == null))
			{
				operator++;
			}
		}
		
		return operator;
	}
	
	/**
	 * Compute the average call time and the status in the list_call
	 * @param list_call
	 * 					A calls list
	 */
	public void calculateCallTimeAndStatus(List<Call> list_call)
	{
		enabledDataLabel();
		// Search and calculate the call time
		for (Call call : list_call) 
		{	
			total_sec = total_sec + call.getDuration();
			
			if(call.getOne_status().getIdStatus() == 1)
			{
				call_success++;
			}
			else
			{
				call_failed++;
			}
		}
		// Set the label for call time
		if(list_call.size() != 0)
		{
			total_sec = total_sec / list_call.size();
		}
		
		mainView.getLblCallTime().setText(convertSecToMin(total_sec));
		// Set the failed call label
		mainView.getLblFailedCall().setText(Integer.toString(call_failed));
		// Set the success call label
		mainView.getLblSuccessCall().setText(Integer.toString(call_success));
	}
	
	/**
	 * Convert second of a call
	 * @param Second Seconds to convert
	 * @return String formated like the : MM:SS
	 */
	public String convertSecToMin(int Second)
	{
		int Minutes = 0, finalSeconds = 0;
		Minutes = (Second % 3600) / 60;
		finalSeconds = Second % 60;
		
		return String.format("%02d", Minutes) + ":" + String.format("%02d", finalSeconds);
	}
	
	/**
	 * Compare if two users lists are equals
	 * @param list_one One user list to compare
	 * @param list_two A second user list to compare
	 * @return Boolean True if the list are equals, false if they are not
	 */
	public Boolean compareTwoUserList(List<User> list_one, List<User> list_two)
	{
		Boolean isEqual = false;
		int i = 0;
		
		if (list_one == null && list_two == null){
			isEqual = false;
	    }

	    if((list_one == null && list_two != null) 
	      || list_one != null && list_two == null
	      || list_one.size() != list_two.size()){
	    	isEqual = false;
	    }
	    
	    for(User user_one : list_one)
	    {
	    	for(User user_two : list_two)
	    	{
	    		if(user_one.toString().equals(user_two.toString()))
	    		{
	    			i++;
	    		}
	    	}
	    }
	    
	    if(i == list_one.size() && i == list_two.size())
	    {
	    	isEqual = true;
	    }
		
		return isEqual;
	}
	
	/**
	 * Compare if two calls lists are equals
	 * @param list_one One call list to compare
	 * @param list_two A second call list to compare
	 * @return Boolean True if the list are equals, false if they are not
	 */
	public Boolean compareTwoCallList(List<Call> list_one, List<Call> list_two)
	{
		Boolean isEqual = false;
		int i = 0;
		
		if (list_one == null && list_two == null){
			isEqual = false;
	    }

	    if((list_one == null && list_two != null) 
	      || list_one != null && list_two == null
	      || list_one.size() != list_two.size()){
	    	isEqual = false;
	    }
	    
	    for(Call call_one : list_one)
	    {
	    	for(Call call_two : list_two)
	    	{
	    		if(call_one.toString().equals(call_two.toString()))
	    		{
	    			i++;
	    		}
	    	}
	    }
	    
	    if(i == list_one.size() && i == list_two.size())
	    {
	    	isEqual = true;
	    }
	    
		return isEqual;
	}

	@Override
	public void mousePressed(MouseEvent e) {
		
	}

	@Override
	public void mouseReleased(MouseEvent e) {
		
	}

	@Override
	public void mouseEntered(MouseEvent e) {
		
	}

	@Override
	public void mouseExited(MouseEvent e) {
		
	}
}
