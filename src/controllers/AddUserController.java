package controllers;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import java.util.List;

import model.DAO.DAO;
import model.DAO.RoleDAO;
import model.DAO.UserDAO;
import model.classes.Role;
import model.classes.User;
import views.AddUserView;

/**
 * AddUserController is the class which manage the event from the view to add a user
 * 
 * @author Clement AZIBEIRO
 * @version 1.0
 */
public class AddUserController implements ActionListener {
	
	private AddUserView add_user_view;
	private DAO<User> user_dao = new UserDAO();
	private DAO<Role> role_dao = new RoleDAO();
	// Get all the role in the DB
	private List<Role> role_list = role_dao.recupAll();
	// Get all supervisors in the DB
	private List<User> supervisors_list = getSupervisors();
	
	/**
	 * Constructor of the AddUserController class
	 * @param add_user_view View to add a user
	 */
	public AddUserController (AddUserView add_user_view) 
	{
		this.add_user_view = add_user_view;
		
		// Fill the Combo Box Role with the role_list
		add_user_view.getComboBoxRole().addItem("");
		
		for(Role role : role_list)
		{
			add_user_view.getComboBoxRole().addItem(role.getDescription());
		}
		
		// Fill the Combo Box Supervisors with the supervisors_list
		add_user_view.getComboBoxSupervisor().addItem("");
		
		for(User user : supervisors_list)
		{
			add_user_view.getComboBoxSupervisor().addItem(user.getFirstname() + " " + user.getLastname());
		}
	}
	
	/**
	 * ActionListener override method used to listening action on JButtons
	 */
	@Override
	public void actionPerformed(ActionEvent e) {
		if(e.getSource() == add_user_view.getBtnCancel())
		{
			add_user_view.dispose();
		} 
		else if(e.getSource() == add_user_view.getBtnCreate())
		{
			if(!add_user_view.getTextFieldFirstname().getText().equals("") && add_user_view.getTextFieldFirstname().getText().length() < 20)
			{
				if(!add_user_view.getTextFieldLastname().getText().equals("") && add_user_view.getTextFieldLastname().getText().length() < 20)
				{
					if(!add_user_view.getTextFieldLogin().getText().equals("") && add_user_view.getTextFieldLogin().getText().length() < 30)
					{
						if(!String.valueOf((add_user_view.getPasswordUserField().getPassword())).equals(""))
						{
							if(!add_user_view.getComboBoxRole().getSelectedItem().equals(""))
							{
								User user = null;
								
								// Instance a user with or without supervisor if there is one selected
								user = new User(1,
												add_user_view.getTextFieldFirstname().getText(),
												add_user_view.getTextFieldLastname().getText(),
												add_user_view.getTextFieldLogin().getText(),
												String.valueOf((add_user_view.getPasswordUserField().getPassword())),
												getSelectedRole(),
												getSelectedSupervisor());
								
								if(user != null)
								{
									// Create the user in DB
									user_dao.create(user);
								}
								
								// Destroy the frame
								add_user_view.dispose();
								// Destroy the user
								user = null;
							}
							else
							{
								add_user_view.getLblError().setText("Choose a Role");
								add_user_view.getLblError().setVisible(true);
							}
						}
						else
						{
							add_user_view.getLblError().setText("Enter a password");
							add_user_view.getLblError().setVisible(true);
						}
					}
					else
					{
						add_user_view.getLblError().setText("Enter a login (maximum 30 characters)");
						add_user_view.getLblError().setVisible(true);
					}
				}
				else
				{
					add_user_view.getLblError().setText("Enter a lastname (maximum 20 characters)");
					add_user_view.getLblError().setVisible(true);
				}
			}
			else
			{
				add_user_view.getLblError().setText("Enter a fisrtname (maximum 20 characters)");
				add_user_view.getLblError().setVisible(true);
			}
		}
	}
	
	/**
	 * Method used to get the selected supervisor in the combo box
	 * @return The user selected in the combo box ComboBoxSupervisor
	 */
	public User getSelectedSupervisor()
	{
		User user_selected = null;
		
		for(User user : supervisors_list)
		{
			if(add_user_view.getComboBoxSupervisor().getSelectedItem().equals(user.getFirstname() + " " + user.getLastname()))
			{
				user_selected = user;
			}
		}
		
		return user_selected;
	}
	
	/**
	 * Method used to get the selected role in the combo box
	 * @return The role selected in the combo box ComboBoxRole
	 */
	public Role getSelectedRole()
	{
		Role role_selected = null;
		
		for(Role role : role_list)
		{
			if(add_user_view.getComboBoxRole().getSelectedItem().equals(role.getDescription()))
			{
				role_selected = role;
			}
		}
		
		return role_selected;
	}
	
	/**
	 * Method used to get the all the supervisors
	 * @return A user list with only supervisors
	 */
	public List<User> getSupervisors()
	{
		List<User> user_list;
		user_list = user_dao.recupAll();
		List<User> supervisors_list = new ArrayList<User>();
		
		for(User user : user_list)
		{
			if(!(user.getSupervisor().getFirstname() != null))
			{
				supervisors_list.add(user);
			}
		}
		
		return supervisors_list;
	}
}
