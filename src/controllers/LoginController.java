package controllers;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import encryption.Encryption;
import model.DAO.DAO;
import model.DAO.RolePermissionDAO;
import model.DAO.UserDAO;
import model.classes.RolePermission;
import model.classes.User;
import views.LoginView;
import views.MainView;

/**
 * LoginController is the class which manage the event from the login view
 * 
 * @author Clement AZIBEIRO
 * @version 1.0
 */
public class LoginController implements ActionListener {

	LoginView loginView;

	/**
	 * Constructor of the LoginController class
	 * @param loginView The login view
	 */
	public LoginController(LoginView loginView) {
		this.loginView = loginView;
	}
	
	/**
	 * ActionListener override method used to listening action on JButtons
	 */
	@Override
	public void actionPerformed(ActionEvent e) 
	{
		DAO<User> user_dao = new UserDAO();
		User good_user = ((UserDAO) user_dao).findUser(loginView.getTxtLogin().getText(),
				Encryption.encryptThisString(String.valueOf((loginView.getTxtPassword().getPassword()))));
		
		if (good_user != null) 
		{
			// Test if the button login is pressed
			if (e.getSource() == loginView.getBtnLogin()) 
			{
				if (good_user.getLogin().equals(loginView.getTxtLogin().getText())) 
				{
					if (good_user.getPassword().equals(Encryption.encryptThisString(String.valueOf((loginView.getTxtPassword().getPassword()))))) 
					{
						// Establish the connection with the DB
						DAO<RolePermission> role_permission_dao = new RolePermissionDAO();
						// Get the permission read of the user trying to login
						RolePermission permission_creation_connected_user = ((RolePermissionDAO) role_permission_dao).readWithPermission(Integer.toString(good_user.getUser_role().getIdRole()), "1");
						// When the user have the permission read
						if(permission_creation_connected_user != null)
						{
							// Dispose the login view
							loginView.dispose();
							try {
								// Instance the main view and controller
								MainView mainView = new MainView();
								mainView.setVisible(true);
								MainController mainController = new MainController(mainView, good_user);
								
								// Listening action on all the buttons and table of the main view
								mainView.getBtnCallTime().addActionListener(mainController);
								mainView.getBtnTotalCall().addActionListener(mainController);
								mainView.getBtnSuccessCall().addActionListener(mainController);
								mainView.getBtnFailedCall().addActionListener(mainController);
								mainView.getBtnOperators().addActionListener(mainController);
								mainView.getBtnBack().addActionListener(mainController);
								mainView.getBtnAddUser().addActionListener(mainController);
								mainView.getRdbtnDate().addActionListener(mainController);
								mainView.getRdbtnToday().addActionListener(mainController);
								mainView.getRdbtnMonth().addActionListener(mainController);
								mainView.getRdbtnThisYear().addActionListener(mainController);
								mainView.getBtnEditUser().addActionListener(mainController);
								mainView.getBtnDeleteUser().addActionListener(mainController);
								mainView.getBtnDisconnect().addActionListener(mainController);
								mainView.getBtnQuit().addActionListener(mainController);
								mainView.getTable().addMouseListener(mainController);
								
							} catch (Exception ex) {
								ex.printStackTrace();
							}
						}
						else
						{
							loginView.getLblError().setText("No permission to connect");
							loginView.getLblError().setVisible(true);
						}
					}
				}
			} 
		} 
		else 
		{
			loginView.getLblError().setText("Bad login or password");
			loginView.getLblError().setVisible(true);
		}
	}
}
