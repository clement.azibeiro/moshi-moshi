package controllers;

import java.awt.EventQueue;

import views.LoginView;

/**
 * Main class of Moshi-moshi, this is where it all begins.
 * 
 * @author Clement AZIBEIRO
 * @version 1.0
 */
public class Main {
	/**
	 * Entry point of the program, launch the login view
	 * @param args Arguments
	 */
	public static void main(String args[]) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					// Instance login view and controller
					LoginView loginView = new LoginView();
					loginView.setVisible(true);
					LoginController loginController = new LoginController(loginView);
					// Listening event from the button login
					loginView.getBtnLogin().addActionListener(loginController);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}
}
