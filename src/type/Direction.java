package type;

/**
 * Direction enumeration type is used to specify call direction
 * 
 * @author Clement AZIBEIRO
 * @version 1.0
 */
public enum Direction {
	In, Out;
}
