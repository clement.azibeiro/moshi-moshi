package model.DAO;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

/**
 * ConnectionPostgreSql class is used to establish a secure connection with postgresql database
 * 
 * @author Clement AZIBEIRO
 * @version 1.0
 */
public class ConnectionPostgreSql {

	static private String url = "jdbc:postgresql://%%URL%%:%%PORT%%/%%DBNAME%%?ssl=true&sslfactory=org.postgresql.ssl.NonValidatingFactory";
	static private String user = "%%USER%%";
	static private String password = "%%PASSWORD%%";
	static private Connection connect;

	/**
	 * Establish the connection with the database
	 * @return A connection to the database
	 */
	public static Connection getInstance() {
		if (connect == null) {
			try {
				connect = DriverManager.getConnection(url, user, password);
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
		return connect;
	}
}
