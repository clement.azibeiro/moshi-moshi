package model.DAO;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import model.classes.Role;

/**
 * RoleDAO, inherits DAO class, is used to get role data from database
 * 
 * @author Clement AZIBEIRO
 * @version 1.0
 */
public class RoleDAO extends DAO<Role> {

	@Override
	/**
	 * Get all roles from the database
	 * @return A role list
	 */
	public List<Role> recupAll() {
		// Definition of the list returned
		List<Role> role_list = new ArrayList<Role>();

		// Declaration of the object used for the sql query
		try {
			
			Statement query = this.conn.createStatement();

			ResultSet cursor = query.executeQuery("SELECT * FROM \"moshi_moshi\".\"Role\"");

			while (cursor.next()) {
				Role oneRole= new Role(cursor.getInt("idRole"),
						cursor.getString("description"));
				
				role_list.add(oneRole);
			}
			cursor.close();
			query.close();

		} catch (SQLException e) {
			e.printStackTrace();
		}

		return role_list;
	}

	@Override
	/**
	 * Create a new role in the database
	 * @param obj A role to create
	 */
	public void create(Role obj) {
		try {
			PreparedStatement prepare = this.conn
					.prepareStatement("INSERT INTO \"moshi_moshi\".\"Role\"(\"description\") VALUES (?)");
			
			prepare.setString(1, obj.getDescription());

			prepare.executeUpdate();

		} catch (SQLException e) {
			e.printStackTrace();
		}
	}

	@Override
	/**
	 * Get a role from the database
	 * @param pk Primary key of a role
	 * @return A role
	 */
	public Role read(String pk) {
		try {
			ResultSet result = this.conn.createStatement(ResultSet.TYPE_SCROLL_INSENSITIVE, ResultSet.CONCUR_UPDATABLE)
					.executeQuery("SELECT * FROM \"moshi_moshi\".\"Role\" WHERE \"Role\".\"idRole\" = '" + pk + "'");
			if (result.first()) {
				Role oneRole = new Role(result.getInt("idRole"),
						result.getString("description"));
				
				return oneRole;
			}
		}

		catch (SQLException e) {
			e.printStackTrace();
		}
		return null;
	}

	@Override
	/**
	 * Update a role in the database
	 * @param obj A role to update
	 */
	public void update(Role obj) {
		try {
			this.conn.createStatement(ResultSet.TYPE_SCROLL_INSENSITIVE, ResultSet.CONCUR_UPDATABLE)
					.executeUpdate("UPDATE \"moshi_moshi\".\"Role\" SET \"description\" = '" + obj.getDescription()
					+ "' WHERE \"idRole\" = '" + obj.getIdRole() + "'");
		} catch (SQLException e) {
			e.printStackTrace();
		}
		
	}

	@Override
	/**
	 * Delete a role in the database
	 * @param obj A role to delete
	 */
	public void delete(Role obj) {
		try {
			this.conn.createStatement(ResultSet.TYPE_SCROLL_INSENSITIVE, ResultSet.CONCUR_UPDATABLE)
					.executeUpdate("DELETE FROM \"moshi_moshi\".\"Role\" WHERE \"Role\".\"idRole\" = '"
							+ obj.getIdRole() + "'");
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}
}
