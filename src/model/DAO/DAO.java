package model.DAO;

import java.sql.Connection;
import java.util.List;

/**
 * Abstract class DAO
 * 
 * @author Clement AZIBEIRO
 * @version 1.0
 * @param <T> An object
 */
public abstract class DAO<T> {

	// Attribute(s)
	/**
	 * Attribute public 'conn' establish the connection with the database
	 */
	public Connection conn = ConnectionPostgreSql.getInstance();

	// Methods
	/**
	 * Abstract method use to get all data
	 * @return A list of object
	 */
	public abstract List<T> recupAll();

	/**
	 * Abstract method use to create data
	 * 
	 * @param obj Instance object of the class T
	 */
	public abstract void create(T obj);

	/**
	 * Abstract method use to read data
	 * 
	 * @param pk  String : Primary key
	 * @return An object
	 */
	public abstract T read(String pk);

	/**
	 * Abstract method use to update data
	 * 
	 * @param obj Instance object of the class T
	 */
	public abstract void update(T obj);

	/**
	 * Abstract method use to delete data
	 * 
	 * @param obj Instance object of the class T
	 */
	public abstract void delete(T obj);
}
