package model.DAO;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import model.classes.Person;

/**
 * PersonDAO, inherits DAO class, is used to get person data from database
 * 
 * @author Clement AZIBEIRO
 * @version 1.0
 */
public class PersonDAO extends DAO<Person> {
	
	@Override
	/**
	 * Get all the person in the database
	 * @return A person list
	 */
	public List<Person> recupAll() {
		// Definition of the list returned
		List<Person> person_list = new ArrayList<Person>();

		// Declaration of the object used for the sql query
		try {

			Statement query = this.conn.createStatement();

			ResultSet cursor = query.executeQuery("SELECT * FROM \"moshi_moshi\".\"Person\"");

			while (cursor.next()) {
				Person onePerson = new Person(cursor.getInt("idPerson"), cursor.getString("firstname"),
						cursor.getString("lastname"));

				person_list.add(onePerson);
			}
			cursor.close();
			query.close();

		} catch (SQLException e) {
			e.printStackTrace();
		}

		return person_list;
	}

	@Override
	/**
	 * Create a new person in the database
	 * @param obj A person to create
	 */
	public void create(Person obj) {
		try {
			PreparedStatement prepare = this.conn
					.prepareStatement("INSERT INTO \"moshi_moshi\".\"Person\"(\"firstname\", \"lastname\")"
									+ "VALUES (?,?)");
			
			prepare.setString(1, obj.getFirstname());
			prepare.setString(2, obj.getLastname());

			prepare.executeUpdate();

		} catch (SQLException e) {
			e.printStackTrace();
		}
	}

	@Override
	/**
	 * Get a person from the database
	 * @param pk Primary key of a person
	 * @return A person
	 */
	public Person read(String pk) {

		try {
			ResultSet result = this.conn.createStatement(ResultSet.TYPE_SCROLL_INSENSITIVE, ResultSet.CONCUR_UPDATABLE)
					.executeQuery(
							"SELECT * FROM \"moshi_moshi\".\"Person\" WHERE \"Person\".\"idPerson\" = '" + pk + "'");
			if (result.first()) {
				Person onePerson = new Person(result.getInt("idPerson"), result.getString("firstname"),
						result.getString("lastname"));

				return onePerson;
			}
		}

		catch (SQLException e) {
			e.printStackTrace();
		}
		return null;
	}

	@Override
	/**
	 * Update a person in the database
	 * @param obj A person to update
	 */
	public void update(Person obj) {
		try {
			this.conn.createStatement(ResultSet.TYPE_SCROLL_INSENSITIVE, ResultSet.CONCUR_UPDATABLE)
					.executeUpdate("UPDATE \"moshi_moshi\".\"Person\" SET \"firstname\" = '" + obj.getFirstname()
							+ "', \"lastname\" = '" + obj.getLastname() + "' WHERE \"idPerson\" = '" + obj.getIdPerson()
							+ "'");
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}

	@Override
	/**
	 * Delete a person in the database
	 * @param obj A person to delete
	 */
	public void delete(Person obj) {
		try {
			this.conn.createStatement(ResultSet.TYPE_SCROLL_INSENSITIVE, ResultSet.CONCUR_UPDATABLE)
					.executeUpdate("DELETE FROM \"moshi_moshi\".\"Person\" WHERE \"Person\".\"idPerson\" = '"
							+ obj.getIdPerson() + "'");
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}
}
