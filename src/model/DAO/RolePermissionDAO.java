package model.DAO;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import model.classes.Permission;
import model.classes.Role;
import model.classes.RolePermission;

/**
 * RolePermissionDAO, inherits DAO class, is used to get role permission data from database
 * 
 * @author Clement AZIBEIRO
 * @version 1.0
 */
public class RolePermissionDAO extends DAO<RolePermission>{

	@Override
	/**
	 * Get all the role permission in the database
	 * @return A list of role permission
	 */
	public List<RolePermission> recupAll() {
		// Definition of the list returned
		List<RolePermission> RolePermission_list = new ArrayList<RolePermission>();

		// Declaration of the object used for the sql query
		try {

			Statement query = this.conn.createStatement();

			ResultSet cursor = query.executeQuery("SELECT rp.\"idRole\", rp.\"idPermission\", r.\"description\" AS \"role_description\", p.\"name\" AS \"permission_name\" " +
					"FROM \"moshi_moshi\".\"RolePermission\" rp\n" + 
					"INNER JOIN \"moshi_moshi\".\"Role\" r ON r.\"idRole\" = rp.\"idRole\"\n" + 
					"INNER JOIN \"moshi_moshi\".\"Permission\" p ON p.\"idPermission\" = rp.\"idPermission\"");

			while (cursor.next()) {
				Role oneRole = new Role(cursor.getInt("idRole"), cursor.getString("role_description"));
				
				Permission onePermission = new Permission(cursor.getInt("idPermission"), cursor.getString("permission_name"));
				
				RolePermission oneRolePermission = new RolePermission(oneRole, onePermission);

				RolePermission_list.add(oneRolePermission);
			}
			cursor.close();
			query.close();

		} catch (SQLException e) {
			e.printStackTrace();
		}

		return RolePermission_list;
	}

	@Override
	/**
	 * Create a new role permission in the database
	 * @param obj A role permission to create
	 */
	public void create(RolePermission obj) {
		try {
			PreparedStatement prepare = this.conn
					.prepareStatement("INSERT INTO \"moshi_moshi\".\"RolePermission\" VALUES (?, ?)");
			
			prepare.setInt(1, obj.getOne_role().getIdRole());
			prepare.setInt(2, obj.getOne_permission().getIdPermission());

			prepare.executeUpdate();
			
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}

	@Override
	/**
	 * Get a role permission from the database, not used return null
	 * @param pk Primary key of a role permission
	 * @return A role permission
	 */
	public RolePermission read(String pk) {
		
		return null;
	}
	
	@Override
	/**
	 * Update a role permission in the database
	 * @param obj A role permission to update
	 */
	public void update(RolePermission obj) {
		try {
			this.conn.createStatement(ResultSet.TYPE_SCROLL_INSENSITIVE, ResultSet.CONCUR_UPDATABLE)
					.executeUpdate("UPDATE \"moshi_moshi\".\"RolePermission\" SET \"idPermission\" = '" + obj.getOne_permission().getIdPermission() + 
							"' WHERE \"idRole\" = '" + obj.getOne_role().getIdRole()
							+ "'");
			
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}

	@Override
	/**
	 * Delete a role permission in the database
	 * @param obj A role permission to delete
	 */
	public void delete(RolePermission obj) {
		try {
			this.conn.createStatement(ResultSet.TYPE_SCROLL_INSENSITIVE, ResultSet.CONCUR_UPDATABLE)
					.executeUpdate("DELETE FROM \"moshi_moshi\".\"RolePermission\" WHERE \"RolePermission\".\"idRole\" = '"
							+ obj.getOne_role().getIdRole() + "' "
							+ "AND \"RolePermission\".\"idPermission\" = '" + obj.getOne_permission().getIdPermission() +"'");
		} catch (SQLException e) {
			e.printStackTrace();
		}
		
	}
	
	/**
	 * Get a role permission from the database
	 * @param pk_role A role key
	 * @param fk_permission A permission key
	 * @return A role permission
	 */
	public RolePermission readWithPermission(String pk_role, String fk_permission) {
		try {
			ResultSet result = this.conn.createStatement(ResultSet.TYPE_SCROLL_INSENSITIVE, ResultSet.CONCUR_UPDATABLE)
					.executeQuery("SELECT rp.\"idRole\", rp.\"idPermission\", r.\"description\" AS \"role_description\", p.\"name\" AS \"permission_name\" " +
							"FROM \"moshi_moshi\".\"RolePermission\" rp\n" + 
							"INNER JOIN \"moshi_moshi\".\"Role\" r ON r.\"idRole\" = rp.\"idRole\"\n" + 
							"INNER JOIN \"moshi_moshi\".\"Permission\" p ON p.\"idPermission\" = rp.\"idPermission\"" +
							" WHERE rp.\"idRole\" ='" + pk_role +"' AND rp.\"idPermission\" = '" + fk_permission + "'");
			if (result.first()) {
				
				Role oneRole = new Role(result.getInt("idRole"), result.getString("role_description"));
				
				Permission onePermission = new Permission(result.getInt("idPermission"), result.getString("permission_name"));
				
				RolePermission oneRolePermission = new RolePermission(oneRole, onePermission);

				return oneRolePermission;
			}
		}

		catch (SQLException e) {
			e.printStackTrace();
		}
		return null;
	}
}
