package model.DAO;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import model.classes.Status;

/**
 * StatusDAO, inherits DAO class, is used to get status data from database
 * 
 * @author Clement AZIBEIRO
 * @version 1.0
 */
public class StatusDAO extends DAO<Status> {

	@Override
	/**
	 * Get all status from the database
	 * @return A status list
	 */
	public List<Status> recupAll() {
		// Definition of the list returned
		List<Status> status_list = new ArrayList<Status>();

		// Declaration of the object used for the sql query
		try {

			Statement query = this.conn.createStatement();

			ResultSet cursor = query.executeQuery("SELECT * FROM \"moshi_moshi\".\"Status\"");

			while (cursor.next()) {
				Status oneStatus = new Status(cursor.getInt("idStatus"), cursor.getString("description"));

				status_list.add(oneStatus);
			}
			cursor.close();
			query.close();

		} catch (SQLException e) {
			e.printStackTrace();
		}

		return status_list;
	}

	@Override
	/**
	 * Create a new status in the database
	 * @param obj A status to create
	 */
	public void create(Status obj) {
		try {
			PreparedStatement prepare = this.conn
					.prepareStatement("INSERT INTO \"moshi_moshi\".\"Status\"(\"description\")" + "VALUES (?)");

			prepare.setString(1, obj.getDescription());

			prepare.executeUpdate();

		} catch (SQLException e) {
			e.printStackTrace();
		}
	}

	@Override
	/**
	 * Get a status in the database
	 * @param pk Primary key of a status
	 */
	public Status read(String pk) {
		try {
			ResultSet result = this.conn.createStatement(ResultSet.TYPE_SCROLL_INSENSITIVE, ResultSet.CONCUR_UPDATABLE)
					.executeQuery(
							"SELECT * FROM \"moshi_moshi\".\"Status\" WHERE \"Status\".\"idStatus\" = '" + pk + "'");
			if (result.first()) {
				Status oneStatus = new Status(result.getInt("idStatus"), result.getString("description"));

				return oneStatus;
			}
		}

		catch (SQLException e) {
			e.printStackTrace();
		}
		return null;
	}

	@Override
	/**
	 * Update a status in the database
	 * @param obj A status to update
	 */
	public void update(Status obj) {
		try {
			this.conn.createStatement(ResultSet.TYPE_SCROLL_INSENSITIVE, ResultSet.CONCUR_UPDATABLE)
					.executeUpdate("UPDATE \"moshi_moshi\".\"Status\" SET \"description\" = '" + obj.getDescription()
							+ "' WHERE \"idStatus\" = '" + obj.getIdStatus() + "'");
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}

	@Override
	/**
	 * Delete a status in the database
	 * @param obj A status to delete
	 */
	public void delete(Status obj) {
		try {
			this.conn.createStatement(ResultSet.TYPE_SCROLL_INSENSITIVE, ResultSet.CONCUR_UPDATABLE)
					.executeUpdate("DELETE FROM \"moshi_moshi\".\"Status\" WHERE \"Status\".\"idStatus\" = '"
							+ obj.getIdStatus() + "'");
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}
}
