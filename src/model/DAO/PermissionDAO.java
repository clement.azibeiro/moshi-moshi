package model.DAO;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import model.classes.Permission;

/**
 * PermissionDAO, inherits DAO class, is used to get permission data from database
 * 
 * @author Clement AZIBEIRO
 * @version 1.0
 */
public class PermissionDAO extends DAO<Permission> {

	@Override
	/**
	 * Get all the permission data
	 * @return A permissions list
	 */
	public List<Permission> recupAll() {
		// Definition of the list returned
		List<Permission> permission_list = new ArrayList<Permission>();

		// Declaration of the object used for the sql query
		try {

			Statement query = this.conn.createStatement();

			ResultSet cursor = query.executeQuery("SELECT * FROM \"moshi_moshi\".\"Permission\"");

			while (cursor.next()) {
				Permission onePermission = new Permission(cursor.getInt("idPermission"), cursor.getString("name"));

				permission_list.add(onePermission);
			}
			cursor.close();
			query.close();

		} catch (SQLException e) {
			e.printStackTrace();
		}

		return permission_list;
	}

	@Override
	/**
	 * Create a new permission in the database
	 * @param obj A permission to create
	 */
	public void create(Permission obj) {
		try {
			PreparedStatement prepare = this.conn
					.prepareStatement("INSERT INTO \"moshi_moshi\".\"Permission\"(\"name\") VALUES (?)");
			
			prepare.setString(1, obj.getName());

			prepare.executeUpdate();

		} catch (SQLException e) {
			e.printStackTrace();
		}

	}

	@Override
	/**
	 * Get a permisison from the database
	 * 
	 * @param Primary key of a permission
	 * @return A permission
	 */
	public Permission read(String pk) {
		try {
			ResultSet result = this.conn.createStatement(ResultSet.TYPE_SCROLL_INSENSITIVE, ResultSet.CONCUR_UPDATABLE)
					.executeQuery(
							"SELECT * FROM \"moshi_moshi\".\"Permission\" WHERE \"Permission\".\"idPermission\" = '"
									+ pk + "'");
			if (result.first()) {
				Permission onePermission = new Permission(result.getInt("idPermission"), result.getString("name"));

				return onePermission;
			}
		}

		catch (SQLException e) {
			e.printStackTrace();
		}
		return null;
	}

	@Override
	/**
	 * Update a permission in the database
	 * @param obj A Permission to update
	 */
	public void update(Permission obj) {
		try {
			this.conn.createStatement(ResultSet.TYPE_SCROLL_INSENSITIVE, ResultSet.CONCUR_UPDATABLE)
					.executeUpdate("UPDATE \"moshi_moshi\".\"Permission\" SET \"name\" = '" + obj.getName()
							+ "' WHERE \"idPermission\" = '" + obj.getIdPermission() + "'");
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}

	@Override
	/**
	 * Delete a permission in the database
	 * @param obj A permission to delete
	 */
	public void delete(Permission obj) {
		try {
			this.conn.createStatement(ResultSet.TYPE_SCROLL_INSENSITIVE, ResultSet.CONCUR_UPDATABLE).executeUpdate(
					"DELETE FROM \"moshi_moshi\".\"Permission\" WHERE \"Permission\".\"idPermission\" = '" + obj.getIdPermission() + "'");
		} catch (SQLException e) {
			e.printStackTrace();
		}

	}

}
