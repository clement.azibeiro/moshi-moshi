package model.DAO;

import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

import model.classes.Call;
import model.classes.ExternPerson;
import model.classes.Role;
import model.classes.Status;
import model.classes.User;
import type.Direction;

/**
 * CallDAO, inherits DAO class, is used to get call data from database
 * 
 * @author Clement AZIBEIRO
 * @version 1.0
 */
public class CallDAO extends DAO<Call> {
	Date date = new Date(Calendar.getInstance().getTime().getTime());

	/**
	 * Get all the call data
	 * 
	 * @return A list of calls
	 */
	public List<Call> recupAll() {
		// Definition of the list returned
		List<Call> call_list = new ArrayList<Call>();

		// Declaration of the object used for the sql query
		try {

			Statement query = this.conn.createStatement();
			String request = "SELECT \n" + "r.\"idRole\", r.\"description\" AS \"name_role\",\n"
					+ "u.\"idPerson\" AS \"idUser\", u.\"firstname\", u.\"lastname\", u.\"login\", u.\"password\",\n"
					+ "s.\"idStatus\", s.\"description\" AS \"description_status\",\n"
					+ "ep.\"idPerson\" AS \"idExternPerson\", ep.\"firstname\" AS \"firstname_ep\", ep.\"lastname\" AS \"lastname_ep\", ep.\"phoneNumber\",\n"
					+ "c.\"idCall\", c.\"date\", c.\"duration\", c.\"direction\"\n"
					+ "FROM \"moshi_moshi\".\"Call\" c \n"
					+ "INNER JOIN \"moshi_moshi\".\"User\" u ON c.\"idUser\" = u.\"idPerson\"\n"
					+ "INNER JOIN \"moshi_moshi\".\"Role\" r ON u.\"idRole\" = r.\"idRole\"\n"
					+ "INNER JOIN \"moshi_moshi\".\"Status\" s ON c.\"idStatus\" = s.\"idStatus\"\n"
					+ "INNER JOIN \"moshi_moshi\".\"ExternPerson\" ep ON c.\"idExternPerson\" = ep.\"idPerson\"\n"
					+ "ORDER BY \"date\" DESC";

			ResultSet cursor = query.executeQuery(request);

			while (cursor.next()) {
				Date date = new Date(cursor.getDate("date").getTime());

				User user = new User(cursor.getInt("idUser"), cursor.getString("firstname"),
						cursor.getString("lastname"), cursor.getString("login"), cursor.getString("password"),
						new Role(cursor.getInt("idRole"), cursor.getString("name_role")), null);

				Status status = new Status(cursor.getInt("idStatus"), cursor.getString("description_status"));

				ExternPerson extern_person = new ExternPerson(cursor.getInt("idExternPerson"),
						cursor.getString("firstname_ep"), cursor.getString("lastname_ep"),
						cursor.getString("phoneNumber"));

				Call oneCall = new Call(cursor.getInt("idCall"), extern_person, status, user, date,
						cursor.getInt("duration"), Direction.valueOf(cursor.getString("direction")));

				call_list.add(oneCall);
			}
			cursor.close();
			query.close();

		} catch (SQLException e) {
			e.printStackTrace();
		}

		return call_list;
	}

	/**
	 * Get all the call data
	 * 
	 * @param date A call date
	 * @return A list of calls with a specific date
	 */
	public List<Call> recupAllDate(Date date) {
		// Definition of the list returned
		List<Call> call_list = new ArrayList<Call>();

		// Declaration of the object used for the sql query
		try {

			Statement query = this.conn.createStatement();
			String request = "SELECT \n" + "r.\"idRole\", r.\"description\" AS \"name_role\",\n"
					+ "u.\"idPerson\" AS \"idUser\", u.\"firstname\", u.\"lastname\", u.\"login\", u.\"password\",\n"
					+ "s.\"idStatus\", s.\"description\" AS \"description_status\",\n"
					+ "ep.\"idPerson\" AS \"idExternPerson\", ep.\"firstname\" AS \"firstname_ep\", ep.\"lastname\" AS \"lastname_ep\", ep.\"phoneNumber\",\n"
					+ "c.\"idCall\", c.\"date\", c.\"duration\", c.\"direction\"\n"
					+ "FROM \"moshi_moshi\".\"Call\" c \n"
					+ "INNER JOIN \"moshi_moshi\".\"User\" u ON c.\"idUser\" = u.\"idPerson\"\n"
					+ "INNER JOIN \"moshi_moshi\".\"Role\" r ON u.\"idRole\" = r.\"idRole\"\n"
					+ "INNER JOIN \"moshi_moshi\".\"Status\" s ON c.\"idStatus\" = s.\"idStatus\"\n"
					+ "INNER JOIN \"moshi_moshi\".\"ExternPerson\" ep ON c.\"idExternPerson\" = ep.\"idPerson\"\n"
					+ "WHERE c.\"date\" = '" + date + "'"
					+ "ORDER BY c.\"date\" DESC";

			ResultSet cursor = query.executeQuery(request);

			while (cursor.next()) {
				Date dateCall = new Date(cursor.getDate("date").getTime());
				// Call oneCall = new Call( date, cursor.getInt("duration"), );

				User user = new User(cursor.getInt("idUser"), cursor.getString("firstname"),
						cursor.getString("lastname"), cursor.getString("login"), cursor.getString("password"),
						new Role(cursor.getInt("idRole"), cursor.getString("name_role")), null);

				Status status = new Status(cursor.getInt("idStatus"), cursor.getString("description_status"));

				ExternPerson extern_person = new ExternPerson(cursor.getInt("idExternPerson"),
						cursor.getString("firstname_ep"), cursor.getString("lastname_ep"),
						cursor.getString("phoneNumber"));

				Call oneCall = new Call(cursor.getInt("idCall"), extern_person, status, user, dateCall,
						cursor.getInt("duration"), Direction.valueOf(cursor.getString("direction")));

				call_list.add(oneCall);
			}
			cursor.close();
			query.close();

		} catch (SQLException e) {
			e.printStackTrace();
		}

		return call_list;
	}

	/**
	 * Get all the call data
	 * 
	 * @param year_or_month A year or month call
	 * @return A list of calls wit a specific month or year
	 */
	public List<Call> recupAllDate(String year_or_month) {
		// Definition of the list returned
		List<Call> call_list = new ArrayList<Call>();

		// Declaration of the object used for the sql query
		try {
			Statement query = this.conn.createStatement();

			String request = "SELECT \n" + "r.\"idRole\", r.\"description\" AS \"name_role\",\n"
					+ "u.\"idPerson\" AS \"idUser\", u.\"firstname\", u.\"lastname\", u.\"login\", u.\"password\",\n"
					+ "s.\"idStatus\", s.\"description\" AS \"description_status\",\n"
					+ "ep.\"idPerson\" AS \"idExternPerson\", ep.\"firstname\" AS \"firstname_ep\", ep.\"lastname\" AS \"lastname_ep\", ep.\"phoneNumber\",\n"
					+ "c.\"idCall\", c.\"date\", c.\"duration\", c.\"direction\"\n"
					+ "FROM \"moshi_moshi\".\"Call\" c \n"
					+ "INNER JOIN \"moshi_moshi\".\"User\" u ON c.\"idUser\" = u.\"idPerson\"\n"
					+ "INNER JOIN \"moshi_moshi\".\"Role\" r ON u.\"idRole\" = r.\"idRole\"\n"
					+ "INNER JOIN \"moshi_moshi\".\"Status\" s ON c.\"idStatus\" = s.\"idStatus\"\n"
					+ "INNER JOIN \"moshi_moshi\".\"ExternPerson\" ep ON c.\"idExternPerson\" = ep.\"idPerson\"\n";

			if (year_or_month.equals("month")) {
				// Get the month
				int mounth = Calendar.getInstance().get(Calendar.MONTH) + 1;
				request = request + "WHERE date_part('MONTH', c.\"date\") = '" + mounth + "'";
			} else if (year_or_month.equals("year")) {
				// Get the year
				int year = Calendar.getInstance().get(Calendar.YEAR);
				request = request + "WHERE date_part('YEAR', c.\"date\") = '" + year + "'";
			} else {
				return null;
			}
			
			request = request + " ORDER BY c.\"date\" DESC";
			
			ResultSet cursor = query.executeQuery(request);

			while (cursor.next()) {
				Date dateCall = new Date(cursor.getDate("date").getTime());
				// Call oneCall = new Call( date, cursor.getInt("duration"), );

				User user = new User(cursor.getInt("idUser"), cursor.getString("firstname"),
						cursor.getString("lastname"), cursor.getString("login"), cursor.getString("password"),
						new Role(cursor.getInt("idRole"), cursor.getString("name_role")), null);

				Status status = new Status(cursor.getInt("idStatus"), cursor.getString("description_status"));

				ExternPerson extern_person = new ExternPerson(cursor.getInt("idExternPerson"),
						cursor.getString("firstname_ep"), cursor.getString("lastname_ep"),
						cursor.getString("phoneNumber"));

				Call oneCall = new Call(cursor.getInt("idCall"), extern_person, status, user, dateCall,
						cursor.getInt("duration"), Direction.valueOf(cursor.getString("direction")));

				call_list.add(oneCall);
			}
			cursor.close();
			query.close();

		} catch (SQLException e) {
			e.printStackTrace();
		}

		return call_list;
	}

	@Override
	/**
	 * Create a new call in the database
	 * 
	 * @param obj A call to create
	 */
	public void create(Call obj) {
		try {
			PreparedStatement prepare = this.conn.prepareStatement("INSERT INTO \"moshi_moshi\".\"Call\""
					+ "(\"idExternPerson\", \"idStatus\", \"idUser\", \"date\", \"duration\", \"direction\") "
					+ "VALUES (?, ?, ?, ?, ?, ?)");

			prepare.setInt(1, obj.getExtern_person().getIdPerson());
			prepare.setInt(2, obj.getOne_status().getIdStatus());
			prepare.setInt(3, obj.getOperator().getIdPerson());
			prepare.setDate(4, obj.getDate());
			prepare.setInt(5, obj.getDuration());
			prepare.setObject(6, obj.getDirection(), java.sql.Types.OTHER);

			prepare.executeUpdate();

		} catch (SQLException e) {
			e.printStackTrace();
		}
	}

	@Override
	/**
	 * Get a call from the database
	 * 
	 * @param Primary key of a call
	 * @return A call
	 */
	public Call read(String pk) {
		try {
			String request = "SELECT \n" + "r.\"idRole\", r.\"description\" AS \"name_role\",\n"
					+ "u.\"idPerson\" AS \"idUser\", u.\"firstname\", u.\"lastname\", u.\"login\", u.\"password\",\n"
					+ "s.\"idStatus\", s.\"description\" AS \"description_status\",\n"
					+ "ep.\"idPerson\" AS \"idExternPerson\", ep.\"firstname\" AS \"firstname_ep\", ep.\"lastname\" AS \"lastname_ep\", ep.\"phoneNumber\",\n"
					+ "c.\"idCall\", c.\"date\", c.\"duration\", c.\"direction\"\n"
					+ "FROM \"moshi_moshi\".\"Call\" c \n"
					+ "INNER JOIN \"moshi_moshi\".\"User\" u ON c.\"idUser\" = u.\"idPerson\"\n"
					+ "INNER JOIN \"moshi_moshi\".\"Role\" r ON u.\"idRole\" = r.\"idRole\"\n"
					+ "INNER JOIN \"moshi_moshi\".\"Status\" s ON c.\"idStatus\" = s.\"idStatus\"\n"
					+ "INNER JOIN \"moshi_moshi\".\"ExternPerson\" ep ON c.\"idExternPerson\" = ep.\"idPerson\"\n"
					+ "WHERE \"idCall\" = '" + pk + "'";

			ResultSet result = this.conn.createStatement(ResultSet.TYPE_SCROLL_INSENSITIVE, ResultSet.CONCUR_UPDATABLE)
					.executeQuery(request);
			if (result.first()) {

				Date date = new Date(result.getDate("date").getTime());
				// Call oneCall = new Call( date, cursor.getInt("duration"), );

				User user = new User(result.getInt("idUser"), result.getString("firstname"),
						result.getString("lastname"), result.getString("login"), result.getString("password"),
						new Role(result.getInt("idRole"), result.getString("name_role")), null);

				Status status = new Status(result.getInt("idStatus"), result.getString("description_status"));

				ExternPerson extern_person = new ExternPerson(result.getInt("idExternPerson"),
						result.getString("firstname_ep"), result.getString("lastname_ep"),
						result.getString("phoneNumber"));

				Call oneCall = new Call(result.getInt("idCall"), extern_person, status, user, date,
						result.getInt("duration"), Direction.valueOf(result.getString("direction")));

				return oneCall;
			}
		}

		catch (SQLException e) {
			e.printStackTrace();
		}

		return null;
	}
	
	@Override
	/**
	 * Update a call in the database
	 * 
	 * @param A call to update
	 */
	public void update(Call obj) {
		try {
			this.conn.createStatement(ResultSet.TYPE_SCROLL_INSENSITIVE, ResultSet.CONCUR_UPDATABLE)
					.executeUpdate("UPDATE \"moshi_moshi\".\"Call\" SET date = '" + obj.getDate() + "', duration = '"
							+ obj.getDuration() + "', direction = '" + obj.getDirection().toString()
							+ "' WHERE \"idCall\" = '" + obj.getIdCall() + "'");
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}
	
	@Override
	/**
	 * Delete a call in the database
	 * @param A call to delete
	 */
	public void delete(Call obj) {
		try {
			this.conn.createStatement(ResultSet.TYPE_SCROLL_INSENSITIVE, ResultSet.CONCUR_UPDATABLE)
					.executeUpdate("DELETE FROM \"moshi_moshi\".\"Call\" WHERE \"idCall\" = '" + obj.getIdCall() + "'");
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}
}
