package model.DAO;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import model.classes.ExternPerson;

/**
 * ExternPersonDAO, inherits DAO class, is used to get extern person data from database
 * 
 * @author Clement AZIBEIRO
 * @version 1.0
 */
public class ExternPersonDAO extends DAO<ExternPerson> {

	@Override
	/**
	 * Get all the extern person data
	 * 
	 * @return A list of ExternPerson
	 */
	public List<ExternPerson> recupAll() {
		// Definition of the list returned
		List<ExternPerson> extern_person_list = new ArrayList<ExternPerson>();

		// Declaration of the object used for the sql query
		try {

			Statement query = this.conn.createStatement();

			ResultSet cursor = query.executeQuery("SELECT * FROM \"moshi_moshi\".\"ExternPerson\"");

			while (cursor.next()) {
				ExternPerson oneExternPerson = new ExternPerson(cursor.getInt("idPerson"), 
						cursor.getString("firstname"),
						cursor.getString("lastname"),
						cursor.getString("phoneNumber"));

				extern_person_list.add(oneExternPerson);
			}
			cursor.close();
			query.close();

		} catch (SQLException e) {
			e.printStackTrace();
		}

		return extern_person_list;
	}

	@Override
	/**
	 * Create a new extern person in the database
	 * @param obj An extern person to create
	 */
	public void create(ExternPerson obj) {
		try {
			PreparedStatement prepare = this.conn
					.prepareStatement("INSERT INTO \"moshi_moshi\".\"ExternPerson\"(\"firstname\", \"lastname\", \"phoneNumber\")"
							+ "VALUES(?, ?, ?)");
			prepare.setString(1, obj.getFirstname());
			prepare.setString(2, obj.getLastname());
			prepare.setString(3, obj.getPhoneNumber());

			prepare.executeUpdate();

		} catch (SQLException e) {
			e.printStackTrace();
		}

	}

	@Override
	/**
	 * Get an extern person from the database
	 * @param pk Primary key of an extern person
	 * @return ExternPerson An extern person
	 */
	public ExternPerson read(String pk) {
		try {
			ResultSet result = this.conn.createStatement(ResultSet.TYPE_SCROLL_INSENSITIVE, ResultSet.CONCUR_UPDATABLE)
					.executeQuery("SELECT * FROM \"moshi_moshi\".\"ExternPerson\" WHERE \"ExternPerson\".\"idPerson\" = '" + pk + "'");
			if (result.first()) {
				ExternPerson oneExternPerson = new ExternPerson(result.getInt("idPerson"),
						result.getString("firstname"),
						result.getString("lastname"),
						result.getString("phoneNumber"));
				
				return oneExternPerson;
			}
		}

		catch (SQLException e) {
			e.printStackTrace();
		}
		return null;
	}

	@Override
	/**
	 * Update an extern person in the database
	 * @param obj An extern person to update
	 */
	public void update(ExternPerson obj) {
		try {
			this.conn.createStatement(ResultSet.TYPE_SCROLL_INSENSITIVE, ResultSet.CONCUR_UPDATABLE)
				.executeUpdate("UPDATE \"moshi_moshi\".\"ExternPerson\" SET \"firstname\" = '" + obj.getFirstname()
																+ "', \"lastname\" = '" + obj.getLastname()
																+ "', \"phoneNumber\" = '" + obj.getPhoneNumber()
																+ "' WHERE \"idPerson\" = '" + obj.getIdPerson() + "'");
		} catch (SQLException e) {
			e.printStackTrace();
		}

	}

	@Override
	/**
	 * Delete an extern person in the database
	 * @param obj An extern person to delete
	 */
	public void delete(ExternPerson obj) {
		try {
			this.conn.createStatement(ResultSet.TYPE_SCROLL_INSENSITIVE, ResultSet.CONCUR_UPDATABLE).executeUpdate(
					"DELETE FROM \"moshi_moshi\".\"ExternPerson\" WHERE \"ExternPerson\".\"idPerson\" = '" + obj.getIdPerson() + "'");
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}
}
