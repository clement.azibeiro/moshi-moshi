package model.DAO;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import model.classes.Role;
import model.classes.User;

/**
 * UserDAO, inherits DAO class, is used to get user data from database
 * 
 * @author Clement AZIBEIRO
 * @version 1.0
 */
public class UserDAO extends DAO<User>{

	@Override
	/**
	 * Get all the users from the database
	 * @return A user list
	 */
	public List<User> recupAll() {
		// Definition of the list returned
		List<User> user_list = new ArrayList<User>();

		// Declaration of the object used for the sql query
		try {

			Statement query = this.conn.createStatement();

			ResultSet cursor = query.executeQuery("SELECT u.\"idPerson\", u.\"firstname\", u.\"lastname\", u.\"login\", u.\"password\", u.\"idRole\", r.\"description\", u.\"HaveAsSupervisor\" AS \"idSupervisor\", u.\"IsConnected\",\n" + 
					"(SELECT \"firstname\" AS \"firstnameSupervisor\" FROM \"moshi_moshi\".\"User\" WHERE \"idPerson\" = u.\"HaveAsSupervisor\"),\n" + 
					"(SELECT \"lastname\" AS \"lastnameSupervisor\" FROM \"moshi_moshi\".\"User\" WHERE \"idPerson\" = u.\"HaveAsSupervisor\"),\n" + 
					"(SELECT \"login\" AS \"loginSupervisor\" FROM \"moshi_moshi\".\"User\" WHERE \"idPerson\" = u.\"HaveAsSupervisor\"),\n" + 
					"(SELECT \"password\" AS \"passwordSupervisor\" FROM \"moshi_moshi\".\"User\" WHERE \"idPerson\" = u.\"HaveAsSupervisor\"),\n" + 
					"(SELECT \"idRole\" AS \"idRoleSupervisor\" FROM \"moshi_moshi\".\"User\" WHERE \"idPerson\" = u.\"HaveAsSupervisor\"),\n" + 
					"(SELECT \"description\" AS \"descriptionRoleSupervisor\"\n" + 
					"	FROM \"moshi_moshi\".\"Role\" r \n" + 
					"	WHERE  r.\"idRole\" = (SELECT \"idRole\" AS \"idRoleSupervisor\" FROM \"moshi_moshi\".\"User\" WHERE \"idPerson\" = u.\"HaveAsSupervisor\"))\n" + 
					"FROM \"moshi_moshi\".\"User\" u\n" + 
					"INNER JOIN \"moshi_moshi\".\"Role\" r ON r.\"idRole\" = u.\"idRole\""
					+ "ORDER BY \"lastname\"");

			while (cursor.next()) {
				if(Integer.toString(cursor.getInt("idRoleSupervisor")) == "") {
					Role oneRole = new Role(cursor.getInt("idRole"), 
											cursor.getString("description"));
					
					User oneUser = new User(cursor.getInt("idPerson"),
											cursor.getString("firstname"),
											cursor.getString("lastname"),
											cursor.getString("login"),
											cursor.getString("password"),
											oneRole,
											null);
					
					oneUser.setIsConnected(cursor.getBoolean("IsConnected"));
					
					user_list.add(oneUser);
				}
				else 
				{
					Role oneRole = new Role(cursor.getInt("idRole"), 
											cursor.getString("description"));
					
					Role oneRoleSupervisor = new Role(cursor.getInt("idRoleSupervisor"), 
														cursor.getString("descriptionRoleSupervisor"));
					
					User supervisor = new User(cursor.getInt("idSupervisor"),
												cursor.getString("firstnameSupervisor"),
												cursor.getString("lastnameSupervisor"),
												cursor.getString("loginSupervisor"),
												cursor.getString("passwordSupervisor"),
												oneRoleSupervisor,
												null);
					
					User oneUser = new User(cursor.getInt("idPerson"),
											cursor.getString("firstname"),
											cursor.getString("lastname"),
											cursor.getString("login"),
											cursor.getString("password"),
											oneRole,
											supervisor);
					
					oneUser.setIsConnected(cursor.getBoolean("IsConnected"));
					
					user_list.add(oneUser);
				}
			}
			cursor.close();
			query.close();

		} catch (SQLException e) {
			e.printStackTrace();
		}

		return user_list;
	}

	@Override
	/**
	 * Create a new user in the database
	 * @param obj A user to create
	 */
	public void create(User obj) {
		try {
			PreparedStatement prepare = this.conn
					.prepareStatement("INSERT INTO \"moshi_moshi\".\"User\"(\"firstname\", \"lastname\", \"login\", \"password\", \"idRole\", \"HaveAsSupervisor\", \"IsConnected\")"
									+ "VALUES (?,?,?,?,?,?,?)");
			
			prepare.setString(1, obj.getFirstname());
			prepare.setString(2, obj.getLastname());
			prepare.setString(3, obj.getLogin());
			prepare.setString(4, obj.getPassword());
			prepare.setInt(5, obj.getUser_role().getIdRole());
			
			if(obj.getSupervisor() == null) {
				prepare.setNull(6, java.sql.Types.INTEGER);
			}
			else 
			{
				prepare.setInt(6, obj.getSupervisor().getIdPerson());
			}
			
			prepare.setBoolean(7, obj.getIsConnected());

			prepare.executeUpdate();

		} catch (SQLException e) {
			e.printStackTrace();
		}
	}

	@Override
	/**
	 * Get a user from the database
	 * @param pk Primary of a user
	 * @return A user
	 */
	public User read(String pk) {
		try {
			ResultSet result = this.conn.createStatement(ResultSet.TYPE_SCROLL_INSENSITIVE, ResultSet.CONCUR_UPDATABLE)
					.executeQuery(
							"SELECT u.\"idPerson\", u.\"firstname\", u.\"lastname\", u.\"login\", u.\"password\", u.\"idRole\", r.\"description\", u.\"HaveAsSupervisor\" AS \"idSupervisor\", u.\"IsConnected\",\n" + 
							"(SELECT \"firstname\" AS \"firstnameSupervisor\" FROM \"moshi_moshi\".\"User\" WHERE \"idPerson\" = u.\"HaveAsSupervisor\"),\n" + 
							"(SELECT \"lastname\" AS \"lastnameSupervisor\" FROM \"moshi_moshi\".\"User\" WHERE \"idPerson\" = u.\"HaveAsSupervisor\"),\n" + 
							"(SELECT \"login\" AS \"loginSupervisor\" FROM \"moshi_moshi\".\"User\" WHERE \"idPerson\" = u.\"HaveAsSupervisor\"),\n" + 
							"(SELECT \"password\" AS \"passwordSupervisor\" FROM \"moshi_moshi\".\"User\" WHERE \"idPerson\" = u.\"HaveAsSupervisor\"),\n" + 
							"(SELECT \"idRole\" AS \"idRoleSupervisor\" FROM \"moshi_moshi\".\"User\" WHERE \"idPerson\" = u.\"HaveAsSupervisor\"),\n" + 
							"(SELECT \"description\" AS \"descriptionRoleSupervisor\"\n" + 
							"	FROM \"moshi_moshi\".\"Role\" r \n" + 
							"	WHERE  r.\"idRole\" = (SELECT \"idRole\" AS \"idRoleSupervisor\" FROM \"moshi_moshi\".\"User\" WHERE \"idPerson\" = u.\"HaveAsSupervisor\")),\n" + 
							"(SELECT \"IsConnected\" AS \"IsConnectedSupervisor\" FROM \"moshi_moshi\".\"User\" WHERE \"idPerson\" = u.\"HaveAsSupervisor\")\n" + 
							"FROM \"moshi_moshi\".\"User\" u\n" + 
							"INNER JOIN \"moshi_moshi\".\"Role\" r ON r.\"idRole\" = u.\"idRole\"\n" + 
							"WHERE u.\"idPerson\" = '" + pk + "'");
			if (result.first()) {
				if(Integer.toString(result.getInt("idRoleSupervisor")) == "") {
					Role oneRole = new Role(result.getInt("idRole"), 
											result.getString("description"));
					
					User oneUser = new User(result.getInt("idPerson"),
											result.getString("firstname"),
											result.getString("lastname"),
											result.getString("login"),
											result.getString("password"),
											oneRole,
											null);
					
					return oneUser;
				}
				else 
				{
					Role oneRole = new Role(result.getInt("idRole"), 
											result.getString("description"));
					
					Role oneRoleSupervisor = new Role(result.getInt("idRoleSupervisor"), 
														result.getString("descriptionRoleSupervisor"));
					
					User supervisor = new User(result.getInt("idSupervisor"),
												result.getString("firstnameSupervisor"),
												result.getString("lastnameSupervisor"),
												result.getString("loginSupervisor"),
												result.getString("passwordSupervisor"),
												oneRoleSupervisor,
												null);
					supervisor.setIsConnected(result.getBoolean("IsConnectedSupervisor"));
					
					User oneUser = new User(result.getInt("idPerson"),
											result.getString("firstname"),
											result.getString("lastname"),
											result.getString("login"),
											result.getString("password"),
											oneRole,
											supervisor);
					
					oneUser.setIsConnected(result.getBoolean("IsConnected"));
					
					return oneUser;
				}
			}
		}

		catch (SQLException e) {
			e.printStackTrace();
		}
		return null;
	}

	@Override
	/**
	 * Update a user in the database
	 * @param obj A user to update
	 */
	public void update(User obj) {
		String request = "UPDATE \"moshi_moshi\".\"User\" SET "
				+ "\"firstname\" = '" + obj.getFirstname()
				+ "', \"lastname\" = '" + obj.getLastname()
				+ "', \"login\" = '" + obj.getLogin()
				+ "', \"password\" = '" + obj.getPassword()
				+ "', \"idRole\" = '" + obj.getUser_role().getIdRole();
		
				if(obj.getSupervisor() != null)
				{
					request = request + "', \"HaveAsSupervisor\" = " + obj.getSupervisor().getIdPerson();
				}
				else
				{
					request = request + "', \"HaveAsSupervisor\" = NULL";
				}
				
				request = request + 
							", \"IsConnected\" = '" + obj.getIsConnected()
						+ "' WHERE \"idPerson\" = '" + obj.getIdPerson() + "'";
		try {
			this.conn.createStatement(ResultSet.TYPE_SCROLL_INSENSITIVE, ResultSet.CONCUR_UPDATABLE).executeUpdate(request);
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}

	@Override
	/**
	 * Delete a user in the database
	 * @param obj A user to delete
	 */
	public void delete(User obj) {
		try {
			this.conn.createStatement(ResultSet.TYPE_SCROLL_INSENSITIVE, ResultSet.CONCUR_UPDATABLE)
					.executeUpdate("DELETE FROM \"moshi_moshi\".\"User\" WHERE \"User\".\"idPerson\" = '"
							+ obj.getIdPerson() + "'");
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}
	
	/**
	 * Find a user in the database with a login and a password
	 * @param login A login
	 * @param password A password
	 * @return A user find with a login and a password
	 */
	public User findUser(String login, String password)
	{
		try {
			String request = "SELECT u.\"idPerson\", u.\"firstname\", u.\"lastname\", u.\"login\", u.\"password\", u.\"idRole\", r.\"description\", u.\"HaveAsSupervisor\" AS \"idSupervisor\", u.\"IsConnected\",\n" + 
					"(SELECT \"firstname\" AS \"firstnameSupervisor\" FROM \"moshi_moshi\".\"User\" WHERE \"idPerson\" = u.\"HaveAsSupervisor\"),\n" + 
					"(SELECT \"lastname\" AS \"lastnameSupervisor\" FROM \"moshi_moshi\".\"User\" WHERE \"idPerson\" = u.\"HaveAsSupervisor\"),\n" + 
					"(SELECT \"login\" AS \"loginSupervisor\" FROM \"moshi_moshi\".\"User\" WHERE \"idPerson\" = u.\"HaveAsSupervisor\"),\n" + 
					"(SELECT \"password\" AS \"passwordSupervisor\" FROM \"moshi_moshi\".\"User\" WHERE \"idPerson\" = u.\"HaveAsSupervisor\"),\n" + 
					"(SELECT \"idRole\" AS \"idRoleSupervisor\" FROM \"moshi_moshi\".\"User\" WHERE \"idPerson\" = u.\"HaveAsSupervisor\"),\n" + 
					"(SELECT \"description\" AS \"descriptionRoleSupervisor\"\n" + 
					"	FROM \"moshi_moshi\".\"Role\" r \n" + 
					"	WHERE  r.\"idRole\" = (SELECT \"idRole\" AS \"idRoleSupervisor\" FROM \"moshi_moshi\".\"User\" WHERE \"idPerson\" = u.\"HaveAsSupervisor\")),\n" + 
					"(SELECT \"IsConnected\" AS \"IsConnectedSupervisor\" FROM \"moshi_moshi\".\"User\" WHERE \"idPerson\" = u.\"HaveAsSupervisor\")\n" + 
					"FROM \"moshi_moshi\".\"User\" u\n" + 
					"INNER JOIN \"moshi_moshi\".\"Role\" r ON r.\"idRole\" = u.\"idRole\"\n" + 
					"WHERE u.\"login\" = '" + login + "' AND u.\"password\" = '" + password + "'";
			
			ResultSet result = this.conn.createStatement(ResultSet.TYPE_SCROLL_INSENSITIVE, ResultSet.CONCUR_UPDATABLE)
					.executeQuery(request);
			
			if (result.first()) {
				if(Integer.toString(result.getInt("idRoleSupervisor")) == "") {
					Role oneRole = new Role(result.getInt("idRole"), 
											result.getString("description"));
					
					User oneUser = new User(result.getInt("idPerson"),
											result.getString("firstname"),
											result.getString("lastname"),
											result.getString("login"),
											result.getString("password"),
											oneRole,
											null);
					
					return oneUser;
				}
				else 
				{
					Role oneRole = new Role(result.getInt("idRole"), 
											result.getString("description"));
					
					Role oneRoleSupervisor = new Role(result.getInt("idRoleSupervisor"), 
														result.getString("descriptionRoleSupervisor"));
					
					User supervisor = new User(result.getInt("idSupervisor"),
												result.getString("firstnameSupervisor"),
												result.getString("lastnameSupervisor"),
												result.getString("loginSupervisor"),
												result.getString("passwordSupervisor"),
												oneRoleSupervisor,
												null);
					supervisor.setIsConnected(result.getBoolean("IsConnectedSupervisor"));
					
					User oneUser = new User(result.getInt("idPerson"),
											result.getString("firstname"),
											result.getString("lastname"),
											result.getString("login"),
											result.getString("password"),
											oneRole,
											supervisor);
					
					oneUser.setIsConnected(result.getBoolean("IsConnected"));
					
					return oneUser;
				}
			}
		}

		catch (SQLException e) {
			e.printStackTrace();
		}
		return null;
	}
}
