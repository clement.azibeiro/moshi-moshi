package model.classes;

/**
 * Permission class is used to instance object representing a person
 * 
 * @author Clement AZIBEIRO
 * @version 1.0
 */
public class Person {
    // Declarations
    private int idPerson;
    private String firstname;
    private String lastname;

    /**
     * Constructor of the Person class
     * @param idPerson The id of the person
     * @param firstname The firstname of the person
     * @param lastname The lastname of the person
     */
    public Person(int idPerson, String firstname, String lastname) {
	this.idPerson = idPerson;
	this.firstname = firstname;
	this.lastname = lastname;
    }
    
    /**
     * Constructor of the Person class
     * @param existing_person A Person class instance
     */
    public Person(Person existing_person) {
    	this(existing_person.getIdPerson(),
    			existing_person.getFirstname(),
    			existing_person.getLastname());
    }

    // Getters
    /**
     * @return The current person id
     */
    public int getIdPerson() {
	return this.idPerson;
    }
    
    /**
     * @return The current person firstname
     */
    public String getFirstname() {
	return this.firstname;
    }
    
    /**
     * @return The current person lastname
     */
    public String getLastname() {
	return this.lastname;
    }

    // Setters
    /**
     * @param idPerson The new person id
     */
    public void setIdPerson(int idPerson) {
	this.idPerson = idPerson;
    }
    
    /**
     * @param firstname The new person firstname
     */
    public void setFirstname(String firstname) {
	this.firstname = firstname;
    }
    
    /**
     * @param lastname The new person lastname
     */
    public void setLastname(String lastname) {
	this.lastname = lastname;
    }
    
    /**
     * Override of the method to string, show all properties of an instance of Person class
     */
	@Override
	public String toString() {
		return "Person [idPerson=" + idPerson + ", firstname=" + firstname + ", lastname=" + lastname + "]";
	}
}
