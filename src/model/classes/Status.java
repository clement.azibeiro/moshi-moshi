package model.classes;

/**
 * Status class is used to instance object representing a call status
 * 
 * @author Clement AZIBEIRO
 * @version 1.0
 */
public class Status {
    // Declarations
	/*
	 * idStatus :
	 * 		1 = Answered
	 * 		2 = Unanswered call
	 * 		3 = Unsuccessful call
	 */
    private int idStatus;
    private String description;

    /**
     * Constructor of the Status class
     * @param idStatus Status id
     * @param description Status description
     */
    // Constructor
    public Status(int idStatus, String description) {
	this.idStatus = idStatus;
	this.description = description;
    }
    
    // Getters
    /**
     * @return The current status id
     */
    public int getIdStatus() {
	return idStatus;
    }

    /**
     * @return The current status description
     */
    public String getDescription() {
	return description;
    }

    // Setters
    /**
     * @param idStatus The new id status
     */
    public void setIdStatus(int idStatus) {
	this.idStatus = idStatus;
    }

    /**
     * @param description The new status description
     */
    public void setDescription(String description) {
	this.description = description;
    }
    
    // Method
    /**
     * Override of the method to string, show all properties of an instance of Status class
     */
	@Override
	public String toString() {
		return "Status [idStatus=" + idStatus + ", description=" + description + "]";
	}
}