package model.classes;

/**
 * ExternPerson class, inherits Person class, is used to instance object representing the extern person of a call
 * 
 * @author Clement AZIBEIRO
 * @version 1.0
 */
public class ExternPerson extends Person {
    // Declaration
    private String phoneNumber;

    /**
     * Constructor of the ExternPerson class, calling the Person class constructor
     * @param idPerson An id of the extern person
     * @param firstname The firstname of the extern person
     * @param lastname The lastname of the extern person
     * @param phoneNumber The phone number of the extern person
     */
    public ExternPerson(int idPerson, String firstname, String lastname, String phoneNumber) {
	super(idPerson, firstname, lastname);
	this.phoneNumber = phoneNumber;
    }

    // Getter
    /**
     * @return The current phone number
     */
    public String getPhoneNumber() {
	return this.phoneNumber;
    }

    // Setter
    /**
     * @param phoneNumber The new phone number
     */
    public void setPhoneNumber(String phoneNumber) {
	this.phoneNumber = phoneNumber;
    }
    
    // Method
    /**
     * Override of the method to string, show all properties of an instance of ExterPerson class
     */
    @Override
	public String toString() {
		return super.toString() + " ExternPerson [phoneNumber=" + phoneNumber + "]";
	}
}
