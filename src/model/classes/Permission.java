package model.classes;

/**
 * Permission class is used to instance object representing the permission of a role
 * 
 * @author Clement AZIBEIRO
 * @version 1.0
 */
public class Permission {
	// Declarations
	/*
	 * 1 = Read 2 = Modification 3 = Creation 4 = Delete
	 * 
	 */
	private int idPermission;
	private String name;

	/**
	 * Constructor of the Permission class
	 * 
	 * @param idPermission The id of the permission
	 * @param name         The name of the permission
	 */
	// Constructor
	public Permission(int idPermission, String name) {
		this.idPermission = idPermission;
		this.name = name;
	}

	// Getters
	/**
	 * @return The current id
	 */
	public int getIdPermission() {
		return this.idPermission;
	}
	
	/**
	 * @return The current name
	 */
	public String getName() {
		return this.name;
	}

	// Setters
	/**
	 * @param idPermission The new id
	 */
	public void setIdPermission(int idPermission) {
		this.idPermission = idPermission;
	}
	
	/**
	 * @param name The new name
	 */
	public void setName(String name) {
		this.name = name;
	}

	// Method
	/**
	 * Override of the method to string, show all properties of an instance of Permission class
	 */
	@Override
	public String toString() {
		return "Permission [idPermission=" + idPermission + ", name=" + name + "]";
	}
}
