package model.classes;

/**
 * User class, inherits Person class, is used to instance object representing an operator or a supervisor
 * 
 * @author Clement AZIBEIRO
 * @version 1.0
 */
public class User extends Person {
    // Declarations
    private String login;
    private String password;
    private Role user_role;
    private User supervisor;
    private boolean isConnected;

    // Constructors
    /**
     * Constructor of the User class
     * @param person An instance of the Person class
     * @param login The user login
     * @param password The user password
     * @param user_role An instance of the Role class, the user role
     * @param supervisor An instance of the User class, The operator supervisor
     */
    public User(Person person,
    		String login,
    		String password,
    		Role user_role,
    		User supervisor) {
		super(person);
		this.login = login;
		this.password = password;
		this.user_role = user_role;
		this.supervisor = supervisor;
    	this.isConnected = false;
    }
    
    /**
     * Constructor of the User class
     * @param idPerson A user id
     * @param firstname A user firstname
     * @param lastname A user lastname
     * @param login A user login
     * @param password A user password
     * @param user_role An instance of the Role class, a user role
     * @param supervisor An instance of the User class, a operator supervisor
     */
    public User(int idPerson,
    		String firstname,
    		String lastname,
    		String login,
    		String password,
    		Role user_role,
    		User supervisor) {
    			this(new Person(idPerson, firstname, lastname),
    			login,
    			password,
    			user_role,
    			supervisor);
    }
    

    // Getters
    /**
     * @return The current user login
     */
    public String getLogin() {
	return this.login;
    }

    /**
     * @return The current user password
     */
    public String getPassword() {
	return this.password;
    }

    /**
     * @return The current user role
     */
    public Role getUser_role() {
	return this.user_role;
    }

    /**
     * @return The current operator supervisor
     */
    public User getSupervisor() {
	return this.supervisor;
    }

    /**
     * @return The current operator status
     */
    public boolean getIsConnected() {
	return this.isConnected;
    }

    // Setters
    /**
     * @param login The new user login
     */
    public void setLogin(String login) {
	this.login = login;
    }

    /**
     * @param password The new user password
     */
    public void setPassword(String password) {
	this.password = password;
    }

    /**
     * @param user_role The new user role
     */
    public void setUser_role(Role user_role) {
	this.user_role = user_role;
    }

    /**
     * @param one_user The new operator supervisor
     */
    public void setSupervisor(User one_user) {
	this.supervisor = one_user;
    }

    /**
     * @param isConnected The new operator status
     */
    public void setIsConnected(boolean isConnected) {
	this.isConnected = isConnected;
    }
    
    // Method
    /**
     * Override of the method to string, show all properties of an instance of User class
     */
	@Override
	public String toString() {
		if (supervisor != null) {
		return super.toString() + " User [login=" + login + ", password=" + password + ", one_role=" + user_role.toString() + ", supervisor="
				+ supervisor.toString() + ", isConnected=" + isConnected + "]";
		}
		else {
			return super.toString() + " Supervisor [login=" + login + ", password=" + password + ", one_role=" + user_role.toString()
				+ ", isConnected=" + isConnected + "]";
		}
	}
}
