package model.classes;

/**
 * RolePermission class is used to instance object representing the association of a role and a permission
 * 
 * @author Clement AZIBEIRO
 * @version 1.0
 */
public class RolePermission {

    // Declarations
    private Role one_role;
    private Permission one_permission;

    /**
     * Constructor of the RolePermission class
     * @param one_role A role 
     * @param one_permission A permission
     */
    // Constructor
    public RolePermission(Role one_role, Permission one_permission) {
	this.one_role = one_role;
	this.one_permission = one_permission;
    }
    
    // Getters
    /**
     * @return The current role
     */
    public Role getOne_role() {
	return one_role;
    }

    /**
     * @return The current permission
     */
    public Permission getOne_permission() {
	return one_permission;
    }

    // Setters
    /**
     * @param one_role The new role
     */
    public void setOne_role(Role one_role) {
	this.one_role = one_role;
    }

    /**
     * @param one_permission The new permission
     */
    public void setOne_permission(Permission one_permission) {
	this.one_permission = one_permission;
    }
    
    //Method
    /**
     * Override of the method to string, show all properties of an instance of RolePermission class
     */
    @Override
	public String toString() {
		return "RolePermission [one_role=" + one_role.toString() + ", one_permission=" + one_permission.toString() + "]";
	}
}
