package model.classes;

/**
 * Role class is used to instance object representing the role of a user
 * 
 * @author Clement AZIBEIRO
 * @version 1.0
 */
public class Role {
    // Declarations
    private int idRole;
    private String description;

    /**
     * Constructor of the Role class
     * @param idRole A role id
     * @param description A role description
     */
    // Constructor
    public Role(int idRole, String description) {
	this.idRole = idRole;
	this.description = description;
    }

    // Getters
    /**
     * @return The current role id
     */
    public int getIdRole() {
	return this.idRole;
    }

    /**
     * @return The current role description
     */
    public String getDescription() {
	return this.description;
    }

    // Setters
    /**
     * @param idRole The new role id
     */
    public void setIdRole(int idRole) {
	this.idRole = idRole;
    }

    /**
     * @param description The new role description
     */
    public void setDescription(String description) {
	this.description = description;
    }
    
    // Method
    /**
     * Override of the method to string, show all properties of an instance of Role class
     */
	@Override
	public String toString() {
		return "Role [idRole=" + idRole + ", description=" + description + "]";
	}
}
