package model.classes;

import java.sql.Date;

import type.Direction;

/**
 * Call class is used to instance object representing a call
 * 
 * @author Clement AZIBEIRO
 * @version 1.0
 */
public class Call {
	// Declarations
	private int idCall;
	private ExternPerson extern_person;
	private Status call_status;
	private User operator;
	private Date date;
	private int duration;
	private Direction direction;

	/**
	 * Constructor of the Call class
	 * @param idCall An id of the call
	 * @param extern_person An instance of ExternPeson class
	 * @param call_status An instance of Status class
	 * @param operator An instance of User class
	 * @param date An instance of the Date class
	 * @param duration The duration
	 * @param direction The direction (In, Out)
	 */
	public Call(int idCall, ExternPerson extern_person, Status call_status, User operator, Date date, int duration,
			Direction direction) {
		this.idCall = idCall;
		this.extern_person = extern_person;
		this.call_status = call_status;
		this.operator = operator;
		this.date = date;
		this.duration = duration;
		this.direction = direction;
	}
	
	// Getters
	/**
	 * @return The current value of call id
	 */
	public int getIdCall() {
		return idCall;
	}
	
	/**
	 * @return The current extern person of the call
	 */
	public ExternPerson getExtern_person() {
		return extern_person;
	}
	
	/**
	 * @return The status of the call
	 */
	public Status getOne_status() {
		return call_status;
	}
	
	/**
	 * @return The current operator of the call
	 */
	public User getOperator() {
		return operator;
	}
	
	/**
	 * @return The date of the call
	 */
	public Date getDate() {
		return date;
	}
	
	/**
	 * @return The duration of the call
	 */
	public int getDuration() {
		return duration;
	}

	/**
	 * @return The direction of the call
	 */
	public Direction getDirection() {
		return direction;
	}

	// Setters
	/**
	 * @param idCall The new call id
	 */
	public void setIdCall(int idCall) {
		this.idCall = idCall;
	}

	/**
	 * @param extern_person The new extern person
	 */
	public void setExtern_person(ExternPerson extern_person) {
		this.extern_person = extern_person;
	}
	
	/**
	 * @param call_status The new call status
	 */
	public void setCall_status(Status call_status) {
		this.call_status = call_status;
	}
	
	/**
	 * @param operator The new call operator
	 */
	public void setOperator(User operator) {
		this.operator = operator;
	}
	
	/**
	 * @param date The new call date
	 */
	public void setDate(Date date) {
		this.date = date;
	}

	/**
	 * @param duration The new call duration
	 */
	public void setDuration(int duration) {
		this.duration = duration;
	}
	
	/**
	 * @param direction The new call direction
	 */
	public void setDirection(Direction direction) {
		this.direction = direction;
	}

	// Methods
	/**
	 * Override of the method to string, show all properties of an instance of Call class
	 */
	@Override
	public String toString() {
		return "Call [idCall=" + idCall + ", one_extern_person=" + extern_person + ", one_status="
				+ call_status.getDescription() + ", one_user=" + operator.getLogin() + ", date=" + date + ", duration="
				+ duration + ", direction=" + direction.toString() + "]";
	}
}
