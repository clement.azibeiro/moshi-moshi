package views;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.JLabel;
import java.awt.Font;
import javax.swing.JTextField;
import javax.swing.JPasswordField;
import javax.swing.JComboBox;
import javax.swing.JButton;
import java.awt.Color;

/**
 * ModifDelView class, inherits JFrame class, is used to create the view to edit or delete a user
 * 
 * @author Clement AZIBEIRO
 * @version 1.0
 */
public class ModifDelView extends JFrame {

	/*
	 * 
	 */
	private static final long serialVersionUID = 7076212945433420412L;
	private JPanel contentPane;
	private JTextField txtFirstname;
	private JTextField txtLastname;
	private JTextField txtLogin;
	private JPasswordField txtPassword;
	private JLabel lblModifDel;
	private JLabel lblFirstname;
	private JLabel lblLastname;
	private JLabel lblLogin;
	private JLabel lblPassword;
	private JLabel lblRole;
	private JLabel lblSupervisor;
	private JLabel lblUserSelection;
	private JComboBox<String> comboBoxUserSelection;
	private JComboBox<String> comboBoxRole;
	private JComboBox<String> comboBoxSupervisor;
	private JButton btnCancel;
	private JButton btnModifDel;
	private JLabel lblSure;
	
	/**
	 * Constructor of the view to edit or delete a user
	 */
	public ModifDelView() {
		setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		setBounds(100, 100, 400, 384);
		setResizable(false);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		
		lblModifDel = new JLabel("");
		lblModifDel.setFont(new Font("Dialog", Font.BOLD, 18));
		lblModifDel.setBounds(12, 0, 209, 22);
		contentPane.add(lblModifDel);
		
		lblFirstname = new JLabel("Firstname :");
		lblFirstname.setFont(new Font("Dialog", Font.BOLD, 12));
		lblFirstname.setBounds(22, 91, 105, 14);
		contentPane.add(lblFirstname);
		
		txtFirstname = new JTextField();
		txtFirstname.setFont(new Font("Dialog", Font.PLAIN, 11));
		txtFirstname.setColumns(10);
		txtFirstname.setBounds(135, 89, 133, 20);
		contentPane.add(txtFirstname);
		
		txtLastname = new JTextField();
		txtLastname.setFont(new Font("Dialog", Font.PLAIN, 11));
		txtLastname.setColumns(10);
		txtLastname.setBounds(135, 116, 133, 20);
		contentPane.add(txtLastname);
		
		lblLastname = new JLabel("Lastname :");
		lblLastname.setFont(new Font("Dialog", Font.BOLD, 12));
		lblLastname.setBounds(22, 118, 105, 14);
		contentPane.add(lblLastname);
		
		lblLogin = new JLabel("Login :");
		lblLogin.setFont(new Font("Dialog", Font.BOLD, 12));
		lblLogin.setBounds(22, 148, 105, 19);
		contentPane.add(lblLogin);
		
		txtLogin = new JTextField();
		txtLogin.setFont(new Font("Dialog", Font.PLAIN, 11));
		txtLogin.setColumns(10);
		txtLogin.setBounds(135, 148, 133, 20);
		contentPane.add(txtLogin);
		
		txtPassword = new JPasswordField();
		txtPassword.setToolTipText("");
		txtPassword.setBounds(135, 179, 133, 20);
		contentPane.add(txtPassword);
		
		lblPassword = new JLabel("Password :");
		lblPassword.setFont(new Font("Dialog", Font.BOLD, 12));
		lblPassword.setBounds(22, 179, 105, 14);
		contentPane.add(lblPassword);
		
		lblRole = new JLabel("Role :");
		lblRole.setFont(new Font("Dialog", Font.BOLD, 12));
		lblRole.setBounds(22, 211, 105, 14);
		contentPane.add(lblRole);
		
		comboBoxRole = new JComboBox<String>();
		comboBoxRole.setBounds(135, 211, 133, 20);
		contentPane.add(comboBoxRole);
		
		comboBoxSupervisor = new JComboBox<String>();
		comboBoxSupervisor.setBounds(135, 240, 133, 20);
		contentPane.add(comboBoxSupervisor);
		
		lblSupervisor = new JLabel("Supervisor :");
		lblSupervisor.setFont(new Font("Dialog", Font.BOLD, 12));
		lblSupervisor.setBounds(22, 243, 105, 14);
		contentPane.add(lblSupervisor);
		
		lblUserSelection = new JLabel("User selection :");
		lblUserSelection.setBounds(22, 34, 116, 15);
		contentPane.add(lblUserSelection);
		
		comboBoxUserSelection = new JComboBox<String>();
		comboBoxUserSelection.setBounds(156, 31, 133, 20);
		contentPane.add(comboBoxUserSelection);
		
		btnCancel = new JButton("Cancel");
		btnCancel.setBounds(80, 296, 114, 25);
		contentPane.add(btnCancel);
		
		btnModifDel = new JButton("");
		btnModifDel.setBounds(210, 296, 114, 25);
		contentPane.add(btnModifDel);
		
		lblSure = new JLabel("");
		lblSure.setForeground(Color.RED);
		lblSure.setBounds(32, 269, 356, 15);
		contentPane.add(lblSure);
	}

	/**
	 * @return The current label to ask the user if he sure to edit or delte a user
	 */
	public JLabel getLblSure() {
		return lblSure;
	}

	/**
	 * @return The current content pane
	 */
	public JPanel getContentPane() {
		return contentPane;
	}

	/**
	 * @return The current firstname text field
	 */
	public JTextField getTxtFirstname() {
		return txtFirstname;
	}

	/**
	 * @return The current lastname text field
	 */
	public JTextField getTxtLastname() {
		return txtLastname;
	}

	/** 
	 * @return The current login text field
	 */
	public JTextField getTxtLogin() {
		return txtLogin;
	}

	/**
	 * @return The current password text field
	 */
	public JPasswordField getTxtPassword() {
		return txtPassword;
	}

	/**
	 * @return The current update or delete label
	 */
	public JLabel getLblModifDel() {
		return lblModifDel;
	}

	/**
	 * @return The current firstname label
	 */
	public JLabel getLblFirstname() {
		return lblFirstname;
	}

	/**
	 * @return The current lastname label
	 */
	public JLabel getLblLastname() {
		return lblLastname;
	}

	/**
	 * @return The current login label
	 */
	public JLabel getLblLogin() {
		return lblLogin;
	}

	/**
	 * @return The current password label
	 */
	public JLabel getLblPassword() {
		return lblPassword;
	}

	/**
	 * @return The current role label
	 */
	public JLabel getLblRole() {
		return lblRole;
	}

	/**
	 * @return The current supervisor label
	 */
	public JLabel getLblSupervisor() {
		return lblSupervisor;
	}

	/**
	 * @return The current user selection label
	 */
	public JLabel getLblUserSelection() {
		return lblUserSelection;
	}

	/**
	 * @return The current user selection combo box
	 */
	public JComboBox<String> getComboBoxUserSelection() {
		return comboBoxUserSelection;
	}

	/**
	 * @return The current role combo box
	 */
	public JComboBox<String> getComboBoxRole() {
		return comboBoxRole;
	}

	/**
	 * @return The current supervisor combo box
	 */
	public JComboBox<String> getComboBoxSupervisor() {
		return comboBoxSupervisor;
	}

	/**
	 * @return The current cancel button
	 */
	public JButton getBtnCancel() {
		return btnCancel;
	}

	/**
	 * @return The current button to update or delete a user
	 */
	public JButton getBtnModifDel() {
		return btnModifDel;
	}
}
