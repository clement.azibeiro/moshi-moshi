package views;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;

import javax.swing.JLabel;
import java.awt.Font;
import javax.swing.JPasswordField;
import javax.swing.JTextField;
import javax.swing.JComboBox;
import javax.swing.JButton;
import java.awt.Color;

/**
 * AddUserView class, inherits JFrame class, is used to create the view to add a user
 * 
 * @author Clement AZIBEIRO
 * @version 1.0
 */
public class AddUserView extends JFrame {

	private static final long serialVersionUID = 6212874666056480520L;
	private JPanel contentPane;
	private JPasswordField passwordUserField;
	private JTextField textFieldLogin;
	private JTextField textFieldLastname;
	private JTextField textFieldFirstname;
	private JButton btnCancel, btnCreate;
	private JComboBox<String> comboBoxSupervisor;
	private JComboBox<String> comboBoxRole;
	private JLabel lblError;

	/**
	 * Constructor of the view
	 */
	public AddUserView() {
		setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		setBounds(100, 100, 325, 315);
		setTitle("Add user");
		setResizable(false);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		
		JLabel lblAddUser = new JLabel("Add user :");
		lblAddUser.setFont(new Font("Open Sans", Font.BOLD, 18));
		lblAddUser.setBounds(20, 12, 105, 19);
		contentPane.add(lblAddUser);
		
		JLabel lblFirstname = new JLabel("Firstname :");
		lblFirstname.setFont(new Font("Open Sans", Font.BOLD, 12));
		lblFirstname.setBounds(20, 59, 105, 14);
		contentPane.add(lblFirstname);
		
		JLabel lblLastname = new JLabel("Lastname :");
		lblLastname.setFont(new Font("Open Sans", Font.BOLD, 12));
		lblLastname.setBounds(20, 84, 105, 14);
		contentPane.add(lblLastname);
		
		JLabel lblLogin = new JLabel("Login :");
		lblLogin.setFont(new Font("Open Sans", Font.BOLD, 12));
		lblLogin.setBounds(20, 109, 105, 19);
		contentPane.add(lblLogin);
		
		JLabel lblPassword = new JLabel("Password :");
		lblPassword.setFont(new Font("Open Sans", Font.BOLD, 12));
		lblPassword.setBounds(20, 139, 105, 14);
		contentPane.add(lblPassword);
		
		JLabel lblRole = new JLabel("Role :");
		lblRole.setFont(new Font("Open Sans", Font.BOLD, 12));
		lblRole.setBounds(20, 164, 105, 14);
		contentPane.add(lblRole);
		
		JLabel lblSupervisor = new JLabel("Supervisor :");
		lblSupervisor.setFont(new Font("Open Sans", Font.BOLD, 12));
		lblSupervisor.setBounds(20, 189, 105, 14);
		contentPane.add(lblSupervisor);
		
		passwordUserField = new JPasswordField();
		passwordUserField.setBounds(160, 139, 133, 20);
		contentPane.add(passwordUserField);
		
		textFieldLogin = new JTextField();
		textFieldLogin.setFont(new Font("Open Sans", Font.PLAIN, 11));
		textFieldLogin.setBounds(160, 111, 133, 20);
		contentPane.add(textFieldLogin);
		textFieldLogin.setColumns(10);
		
		textFieldLastname = new JTextField();
		textFieldLastname.setFont(new Font("Open Sans", Font.PLAIN, 11));
		textFieldLastname.setBounds(160, 84, 133, 20);
		contentPane.add(textFieldLastname);
		textFieldLastname.setColumns(10);
		
		textFieldFirstname = new JTextField();
		textFieldFirstname.setFont(new Font("Open Sans", Font.PLAIN, 11));
		textFieldFirstname.setBounds(160, 59, 133, 20);
		contentPane.add(textFieldFirstname);
		textFieldFirstname.setColumns(10);
		
		comboBoxRole = new JComboBox<String>();
		comboBoxRole.setBounds(160, 164, 133, 20);
		contentPane.add(comboBoxRole);
		
		comboBoxSupervisor = new JComboBox<String>();
		comboBoxSupervisor.setBounds(160, 189, 133, 20);
		contentPane.add(comboBoxSupervisor);
		
		btnCreate = new JButton("Create");
		btnCreate.setBounds(165, 241, 89, 23);
		contentPane.add(btnCreate);
		
		btnCancel = new JButton("Cancel");
		btnCancel.setBounds(64, 241, 89, 23);
		contentPane.add(btnCancel);
		
		lblError = new JLabel("");
		lblError.setVisible(false);
		lblError.setForeground(Color.RED);
		lblError.setFont(new Font("Open Sans", Font.BOLD, 12));
		lblError.setBounds(10, 216, 260, 14);
		contentPane.add(lblError);
	}

	/**
	 * @return The current content pane
	 */
	public JPanel getContentPane() {
		return contentPane;
	}
	
	/**
	 * @return The current password text field
	 */
	public JPasswordField getPasswordUserField() {
		return passwordUserField;
	}
	
	/**
	 * @return The current login text field
	 */
	public JTextField getTextFieldLogin() {
		return textFieldLogin;
	}
	
	/**
	 * @return The current lastname text field
	 */
	public JTextField getTextFieldLastname() {
		return textFieldLastname;
	}

	/**
	 * @return The current firstname text field
	 */
	public JTextField getTextFieldFirstname() {
		return textFieldFirstname;
	}
	
	/**
	 * @return The current cancel button
	 */
	public JButton getBtnCancel() {
		return btnCancel;
	}

	/**
	 * @return The current create button
	 */
	public JButton getBtnCreate() {
		return btnCreate;
	}
	
	/**
	 * @return The current supervisor combo box
	 */
	public JComboBox<String> getComboBoxSupervisor() {
		return comboBoxSupervisor;
	}
	
	/**
	 * @return The current role combo box
	 */
	public JComboBox<String> getComboBoxRole() {
		return comboBoxRole;
	}

	/**
	 * @return The current error label
	 */
	public JLabel getLblError() {
		return lblError;
	}
}
