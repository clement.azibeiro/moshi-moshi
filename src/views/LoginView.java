package views;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.JPasswordField;
import javax.swing.border.EmptyBorder;
import javax.swing.JLabel;
import javax.swing.JTextField;
import javax.swing.JButton;
import java.awt.Font;
import java.awt.Color;
import java.awt.Component;

/**
 * LoginView class, inherits JFrame class, is used to create the view to login
 * 
 * @author Clement AZIBEIRO
 * @version 1.0
 */
public class LoginView extends JFrame {
	/*
	 * Declarations
	 */
	private static final long serialVersionUID = -1066918809529822008L;
	private JPanel contentPane;
	private JTextField txtLogin;
	private JPasswordField txtPassword;
	private JLabel lblSignIn, lblLogin, lblPassword, lblError;
	private JButton btnLogin;

	/**
	 * Constructor of the login view
	 */
	public LoginView() {
		setTitle("Moshi Moshi");
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 198, 251);
		setResizable(false);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		
		lblSignIn = new JLabel("Sign In");
		lblSignIn.setFont(new Font("Open Sans", Font.BOLD, 16));
		lblSignIn.setBounds(12, 0, 70, 25);
		contentPane.add(lblSignIn);
		
		lblLogin = new JLabel("Login :");
		lblLogin.setFont(new Font("Open Sans", Font.PLAIN, 12));
		lblLogin.setBounds(22, 36, 70, 15);
		contentPane.add(lblLogin);
		
		lblPassword = new JLabel("Password :");
		lblPassword.setFont(new Font("Open Sans", Font.PLAIN, 12));
		lblPassword.setBounds(22, 89, 89, 15);
		contentPane.add(lblPassword);
		
		txtLogin = new JTextField();
		txtLogin.setBounds(46, 58, 114, 19);
		contentPane.add(txtLogin);
		txtLogin.setColumns(10);
		
		txtPassword = new JPasswordField();
		txtPassword.setBounds(46, 109, 114, 19);
		contentPane.add(txtPassword);
		txtPassword.setColumns(10);
		
		btnLogin = new JButton("Login");
		btnLogin.setAlignmentX(Component.CENTER_ALIGNMENT);
		btnLogin.setBounds(32, 168, 117, 25);
		contentPane.add(btnLogin);
		
		lblError = new JLabel();
		lblError.setFont(new Font("Open Sans ExtraBold", Font.PLAIN, 11));
		lblError.setForeground(Color.RED);
		lblError.setVisible(false);
		lblError.setBounds(12, 137, 170, 14);
		contentPane.add(lblError);
		
		getRootPane().setDefaultButton(btnLogin);
	}
	
	// Getters
	/**
	 * @return The current content pane
	 */
	public JPanel getContentPane() {
		return contentPane;
	}
	
	/**
	 * @return The current login text field 
	 */
	public JTextField getTxtLogin() {
		return txtLogin;
	}
	
	/**
	 * @return The current password text field
	 */
	public JPasswordField getTxtPassword() {
		return txtPassword;
	}

	/**
	 * @return The current sign in label
	 */
	public JLabel getLblSignIn() {
		return lblSignIn;
	}

	/**
	 * @return The current login label
	 */
	public JLabel getLblLogin() {
		return lblLogin;
	}

	/**
	 * @return The current password label
	 */
	public JLabel getLblPassword() {
		return lblPassword;
	}

	/**
	 * @return The current login button
	 */
	public JButton getBtnLogin() {
		return btnLogin;
	}

	/**
	 * @return The current error label
	 */
	public JLabel getLblError() {
		return lblError;
	}
}
