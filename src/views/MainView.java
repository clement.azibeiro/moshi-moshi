package views;

import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.border.Border;
import javax.swing.border.EmptyBorder;

import com.toedter.calendar.JDateChooser;

import javax.swing.JRadioButton;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.BorderFactory;
import javax.swing.ButtonGroup;
import javax.swing.JButton;

import java.awt.Color;
import java.awt.Font;

import javax.swing.SwingConstants;
import java.awt.Component;

/**
 * MainView class, inherits JFrame class, is used to create the main view
 * 
 * @author Clement AZIBEIRO
 * @version 1.0
 */
public class MainView extends JFrame {

	/*
	 * Declarations
	 */
	private static final long serialVersionUID = 1434216695759527061L;
	private JPanel contentPane;
	private JRadioButton rdbtnToday, rdbtnMounth, rdbtnThisYear, rdbtnDate;
	private JDateChooser dateChooser;
	private ButtonGroup rdnbuttongroup;
	private JLabel lblDate, lblCallTime, lblOperators, lblSuccessCall, lblFailedCall, lblTotalCall, lblTableTitle;
	private JButton btnTotalCall, btnCallTime, btnFailedCall, btnSuccessCall, btnOperators, btnBack;
	private JScrollPane scrollPane;
	private JTable table;
	Border border = BorderFactory.createLineBorder(Color.BLACK, 1);
	private JButton btnAddUser;
	private JButton btnEditUser;
	private JButton btnDeleteUser;
	private JButton btnDisconnect;
	private JButton btnQuit;

	/**
	 * Constructor of the main view
	 */
	public MainView() {
		setTitle("Moshi Moshi");
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 600, 500);
		setResizable(false);
		contentPane = new JPanel();
		contentPane.setBackground(Color.WHITE);
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		
		rdbtnToday = new JRadioButton("Today");
		rdbtnToday.setFont(new Font("Open Sans", Font.PLAIN, 11));
		rdbtnToday.setBackground(Color.WHITE);
		rdbtnToday.setSelected(true);
		rdbtnToday.setBounds(30, 8, 80, 23);
		contentPane.add(rdbtnToday);
		
		rdbtnMounth = new JRadioButton("This month");
		rdbtnMounth.setFont(new Font("Open Sans", Font.PLAIN, 11));
		rdbtnMounth.setBackground(Color.WHITE);
		rdbtnMounth.setBounds(114, 8, 112, 23);
		contentPane.add(rdbtnMounth);
		
		rdbtnThisYear = new JRadioButton("This year");
		rdbtnThisYear.setFont(new Font("Open Sans", Font.PLAIN, 11));
		rdbtnThisYear.setBackground(Color.WHITE);
		rdbtnThisYear.setBounds(228, 11, 87, 17);
		contentPane.add(rdbtnThisYear);
		
		rdbtnDate = new JRadioButton("");
		rdbtnDate.setBackground(Color.WHITE);
		rdbtnDate.setBounds(317, 8, 26, 23);
		contentPane.add(rdbtnDate);
		
		dateChooser = new JDateChooser();
		dateChooser.setBounds(344, 10, 113, 19);
		contentPane.add(dateChooser);
		
		rdnbuttongroup = new ButtonGroup();
		rdnbuttongroup.add(rdbtnToday);
		rdnbuttongroup.add(rdbtnMounth);
		rdnbuttongroup.add(rdbtnThisYear);
		rdnbuttongroup.add(rdbtnDate);
		
		btnCallTime = new JButton("Call time");
		btnCallTime.setFocusable(false);
		btnCallTime.setFont(new Font("Open Sans", Font.PLAIN, 12));
		btnCallTime.setOpaque(true);
		btnCallTime.setHorizontalAlignment(SwingConstants.CENTER);
		btnCallTime.setVerticalAlignment(SwingConstants.TOP);
		btnCallTime.setForeground(Color.BLACK);
		btnCallTime.setBackground(new Color(0, 191, 255));
		btnCallTime.setBounds(233, 92, 125, 125);
		contentPane.add(btnCallTime);
		
		btnTotalCall = new JButton("Total calls");
		btnTotalCall.setFocusable(false);
		btnTotalCall.setFont(new Font("Open Sans", Font.PLAIN, 12));
		btnTotalCall.setOpaque(true);
		btnTotalCall.setHorizontalAlignment(SwingConstants.CENTER);
		btnTotalCall.setVerticalAlignment(SwingConstants.TOP);
		btnTotalCall.setForeground(Color.BLACK);
		btnTotalCall.setBackground(new Color(0, 191, 255));
		btnTotalCall.setBounds(368, 92, 125, 125);
		contentPane.add(btnTotalCall);
		
		btnFailedCall = new JButton("Failed calls");
		btnFailedCall.setFocusable(false);
		btnFailedCall.setFont(new Font("Open Sans", Font.PLAIN, 12));
		btnFailedCall.setOpaque(true);
		btnFailedCall.setHorizontalAlignment(SwingConstants.CENTER);
		btnFailedCall.setVerticalAlignment(SwingConstants.TOP);
		btnFailedCall.setForeground(Color.BLACK);
		btnFailedCall.setBackground(Color.RED);
		btnFailedCall.setBounds(96, 229, 125, 125);
		contentPane.add(btnFailedCall);
		
		btnSuccessCall = new JButton("Successful calls");
		btnSuccessCall.setFocusable(false);
		btnSuccessCall.setOpaque(true);
		btnSuccessCall.setHorizontalAlignment(SwingConstants.CENTER);
		btnSuccessCall.setVerticalAlignment(SwingConstants.TOP);
		btnSuccessCall.setFont(new Font("Open Sans", Font.PLAIN, 12));
		btnSuccessCall.setForeground(Color.BLACK);
		btnSuccessCall.setBackground(Color.GREEN);
		btnSuccessCall.setBounds(231, 229, 125, 125);
		contentPane.add(btnSuccessCall);
		
		btnOperators = new JButton("Operators");
		btnOperators.setFocusable(false);
		btnOperators.setFont(new Font("Open Sans", Font.PLAIN, 12));
		btnOperators.setOpaque(true);
		btnOperators.setHorizontalAlignment(SwingConstants.CENTER);
		btnOperators.setVerticalAlignment(SwingConstants.TOP);
		btnOperators.setForeground(Color.BLACK);
		btnOperators.setBackground(new Color(0, 191, 255));
		btnOperators.setBounds(368, 229, 125, 125);
		contentPane.add(btnOperators);
		
		lblDate = new JLabel("");
		lblDate.setFont(new Font("Dialog", Font.BOLD, 16));
		lblDate.setOpaque(true);
		lblDate.setHorizontalAlignment(SwingConstants.CENTER);
		lblDate.setBackground(Color.ORANGE);
		lblDate.setForeground(Color.BLACK);
		lblDate.setBorder(border);
		lblDate.setBounds(96, 92, 125, 125);
		contentPane.add(lblDate);
		
		lblTotalCall = new JLabel("");
		lblTotalCall.setAlignmentX(Component.CENTER_ALIGNMENT);
		lblTotalCall.setHorizontalAlignment(SwingConstants.CENTER);
		lblTotalCall.setBackground(Color.WHITE);
		lblTotalCall.setForeground(Color.BLACK);
		lblTotalCall.setFont(new Font("Open Sans", Font.BOLD, 25));
		lblTotalCall.setBounds(302, 39, 125, 125);
		btnTotalCall.add(lblTotalCall);
		
		lblCallTime = new JLabel("");
		lblCallTime.setAlignmentX(Component.CENTER_ALIGNMENT);
		lblCallTime.setHorizontalTextPosition(SwingConstants.CENTER);
		lblCallTime.setHorizontalAlignment(SwingConstants.CENTER);
		lblCallTime.setBackground(Color.WHITE);
		lblCallTime.setForeground(Color.BLACK);
		lblCallTime.setFont(new Font("Open Sans", Font.BOLD, 25));
		lblCallTime.setBounds(167, 39, 125, 125);
		btnCallTime.add(lblCallTime);
		
		lblOperators = new JLabel("");
		lblOperators.setAlignmentX(Component.CENTER_ALIGNMENT);
		lblOperators.setHorizontalAlignment(SwingConstants.CENTER);
		lblOperators.setBackground(Color.WHITE);
		lblOperators.setForeground(Color.BLACK);
		lblOperators.setFont(new Font("Open Sans", Font.BOLD, 25));
		lblOperators.setBounds(302, 176, 125, 125);
		btnOperators.add(lblOperators);
		
		lblSuccessCall = new JLabel("");
		lblSuccessCall.setAlignmentX(Component.CENTER_ALIGNMENT);
		lblSuccessCall.setHorizontalAlignment(SwingConstants.CENTER);
		lblSuccessCall.setBackground(Color.GREEN);
		lblSuccessCall.setForeground(Color.BLACK);
		lblSuccessCall.setFont(new Font("Open Sans", Font.BOLD, 25));
		lblSuccessCall.setBounds(165, 176, 125, 125);
		btnSuccessCall.add(lblSuccessCall);
		
		lblFailedCall = new JLabel("");
		lblFailedCall.setAlignmentX(Component.CENTER_ALIGNMENT);
		lblFailedCall.setHorizontalAlignment(SwingConstants.CENTER);
		lblFailedCall.setBackground(Color.RED);
		lblFailedCall.setForeground(Color.BLACK);
		lblFailedCall.setFont(new Font("Open Sans", Font.BOLD, 25));
		lblFailedCall.setBounds(30, 176, 125, 125);
		btnFailedCall.add(lblFailedCall);
		
		btnBack = new JButton("Back");
		btnBack.setVisible(false);
		btnBack.setBounds(21, 428, 89, 23);
		contentPane.add(btnBack);
		
		table = new JTable();
		table.setAutoResizeMode(JTable.AUTO_RESIZE_OFF);
		table.setRowSelectionAllowed(false);
		table.getTableHeader().setResizingAllowed(false);
		table.getTableHeader().setReorderingAllowed(false);
		table.setFocusable(false);
		table.setBounds(317, 294, 397, 262);
		
		scrollPane = new JScrollPane(table);
		scrollPane.setVisible(false);
		scrollPane.setBounds(10, 81, 562, 335);
		contentPane.add(scrollPane);
		
		lblTableTitle = new JLabel("");
		lblTableTitle.setFont(new Font("Open Sans", Font.BOLD, 18));
		lblTableTitle.setBounds(30, 38, 155, 32);
		lblTableTitle.setVisible(false);
		contentPane.add(lblTableTitle);
		
		btnAddUser = new JButton("Add");
		btnAddUser.setVisible(false);
		btnAddUser.setBounds(190, 49, 70, 25);
		contentPane.add(btnAddUser);
		
		btnEditUser = new JButton("Edit");
		btnEditUser.setVisible(false);
		btnEditUser.setBounds(272, 49, 70, 25);
		contentPane.add(btnEditUser);
		
		btnDeleteUser = new JButton("Delete");
		btnDeleteUser.setVisible(false);
		btnDeleteUser.setBounds(354, 49, 90, 25);
		contentPane.add(btnDeleteUser);
		
		btnDisconnect = new JButton("Disconnect");
		btnDisconnect.setBounds(368, 427, 125, 25);
		contentPane.add(btnDisconnect);
		
		btnQuit = new JButton("Quit");
		btnQuit.setBounds(502, 427, 70, 25);
		contentPane.add(btnQuit);
	}
	
	// Getters
	/**
	 * @return The current content pane
	 */
	public JPanel getContentPane() {
		return contentPane;
	}

	/**
	 * @return The current today radio button
	 */
	public JRadioButton getRdbtnToday() {
		return rdbtnToday;
	}

	/**
	 * @return The current month radio button
	 */
	public JRadioButton getRdbtnMonth() {
		return rdbtnMounth;
	}

	/**
	 * @return The current year radio button
	 */
	public JRadioButton getRdbtnThisYear() {
		return rdbtnThisYear;
	}

	/**
	 * @return The current date radio button
	 */
	public JRadioButton getRdbtnDate() {
		return rdbtnDate;
	}

	/**
	 * @return The current date chooser
	 */
	public JDateChooser getDateChooser() {
		return dateChooser;
	}

	/**
	 * @return The current radio button group
	 */
	public ButtonGroup getRdnbuttongroup() {
		return rdnbuttongroup;
	}

	/**
	 * @return The current data label
	 */
	public JLabel getLblDate() {
		return lblDate;
	}

	/**
	 * @return The current call time button
	 */
	public JButton getBtnCallTime() {
		return btnCallTime;
	}

	/**
	 * @return The current total call button
	 */
	public JButton getBtnTotalCall() {
		return btnTotalCall;
	}

	/**
	 * @return The current failed call button
	 */
	public JButton getBtnFailedCall() {
		return btnFailedCall;
	}

	/**
	 * @return The current success call button
	 */
	public JButton getBtnSuccessCall() {
		return btnSuccessCall;
	}

	/**
	 * @return The current operator button
	 */
	public JButton getBtnOperators() {
		return btnOperators;
	}

	/**
	 * @return The current call time label
	 */
	public JLabel getLblCallTime() {
		return lblCallTime;
	}

	/**
	 * @return The current operator label
	 */
	public JLabel getLblOperators() {
		return lblOperators;
	}

	/**
	 * @return The current success call label
	 */
	public JLabel getLblSuccessCall() {
		return lblSuccessCall;
	}

	/**
	 * @return The current failed call label
	 */
	public JLabel getLblFailedCall() {
		return lblFailedCall;
	}

	/**
	 * @return The current total call label
	 */
	public JLabel getLblTotalCall() {
		return lblTotalCall;
	}

	/**
	 * @return The current back button
	 */
	public JButton getBtnBack() {
		return btnBack;
	}

	/**
	 * @return The current scroll pane
	 */
	public JScrollPane getScrollPane() {
		return scrollPane;
	}

	/**
	 * @return The current table
	 */
	public JTable getTable() {
		return table;
	}

	/**
	 * @return The current table title
	 */
	public JLabel getLblTableTitle() {
		return lblTableTitle;
	}

	/**
	 * @return The current button to add a user
	 */
	public JButton getBtnAddUser() {
		return btnAddUser;
	}

	/**
	 * @return The current button to edit a user
	 */
	public JButton getBtnEditUser() {
		return btnEditUser;
	}

	/**
	 * @return The current button to delete a user
	 */
	public JButton getBtnDeleteUser() {
		return btnDeleteUser;
	}

	/**
	 * @return The current disconnect button
	 */
	public JButton getBtnDisconnect() {
		return btnDisconnect;
	}

	/**
	 * @return The current quit button
	 */
	public JButton getBtnQuit() {
		return btnQuit;
	}
}
