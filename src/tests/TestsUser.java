package tests;

import model.classes.Person;
import model.classes.Role;
import model.classes.User;

/**
 * The test class for the User class
 * 
 * @author Clement AZIBEIRO
 * @version 1.0
 */
public class TestsUser {

	public static void main(String[] args) {
		Role oneRole = new Role(1, "GOD");
		Role twoRole = new Role(2, "TEST");
		
		Person onePerson = new Person(1, "Clement", "AZIBEIRO");
		User supervisor = new User(onePerson, "c.azibeiro", "password", oneRole, null);
		User operator = new User (2, "Hugo", "MERCIER", "h.mercier", "password", twoRole, supervisor);
		
		System.out.println(supervisor.getIdPerson() + " " +
							supervisor.getFirstname() + " " +
							supervisor.getLastname() + " " +
							supervisor.getLogin() + " " +
							supervisor.getPassword() + " " +
							supervisor.getUser_role().toString() + " " +
							supervisor.getSupervisor());
		
		System.out.println(operator.getIdPerson() + " " +
				operator.getFirstname() + " " +
				operator.getLastname() + " " +
				operator.getLogin() + " " +
				operator.getPassword() + " " +
				operator.getUser_role().toString() + " " +
				operator.getSupervisor().toString());
		
		System.out.println("\n" + supervisor.toString());
		System.out.println("\n" + operator.toString());
	}

}
