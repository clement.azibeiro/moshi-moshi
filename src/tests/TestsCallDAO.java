package tests;

import java.sql.Date;
import java.util.Calendar;
import java.util.List;

import model.DAO.CallDAO;
import model.DAO.DAO;
import model.classes.Call;
import model.classes.ExternPerson;
import model.classes.Role;
import model.classes.Status;
import model.classes.User;
import type.Direction;

/**
 * The test class for the CallDAO class
 * 
 * @author Clement AZIBEIRO
 * @version 1.0
 */
public class TestsCallDAO {

	public static void main(String[] args) {
		
		DAO<Call> call_dao = new CallDAO();
		int last_idCall = 0;
		
		User user = new User(1, "prenom", "nom", "p.nom", "password", new Role(2, "test_role"), null);

		Status status = new Status(1, "test status");

		ExternPerson extern_person = new ExternPerson(1, "test_prenom", "test_nom", "0632659856");

		Call oneCall = new Call(2, extern_person, status, user, new Date(Calendar.getInstance().getTime().getTime()),
				155, Direction.In);
		
		// test of the method create
		call_dao.create(oneCall);
		
		// test of the method recupAll
		List<Call> list_call;
		list_call = call_dao.recupAll();
		
		for (Call call : list_call) {
			System.out.println(call.toString());
			
		}
		
		last_idCall = list_call.get(list_call.size() - 1).getIdCall();
		
		// test of the method read
		System.out.println(call_dao.read(Integer.toString(last_idCall)).toString());
		
		oneCall.setDuration(222);
		oneCall.setIdCall(last_idCall);
		
		// test of the method update
		call_dao.update(oneCall);
		System.out.println(call_dao.read(Integer.toString(last_idCall)).toString());
		
		// test of the method delete
		call_dao.delete(oneCall);
	}
}
