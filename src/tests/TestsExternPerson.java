package tests;

import model.classes.ExternPerson;

/**
 * The test class for the ExternPerson class
 * 
 * @author Clement AZIBEIRO
 * @version 1.0
 */
public class TestsExternPerson {

	public static void main(String[] args) {
		ExternPerson oneExternPerson = new ExternPerson(1, "firstname", "lastname", "0611234568");

		System.out.println(oneExternPerson.getIdPerson() + " " + 
							oneExternPerson.getFirstname() + " " + 
							oneExternPerson.getLastname() + " " +
							oneExternPerson.getPhoneNumber());
		
		System.out.println(oneExternPerson.toString());

	}

}
