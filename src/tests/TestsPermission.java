package tests;

import model.classes.Permission;

/**
 * The test class for the Permission class
 * 
 * @author Clement AZIBEIRO
 * @version 1.0
 */
public class TestsPermission {

	public static void main(String[] args) {
		Permission onePermission = new Permission(1, "permission_name");

		System.out.println(onePermission.getIdPermission() + " " + onePermission.getName());
		System.out.println(onePermission.toString());
	}
}
