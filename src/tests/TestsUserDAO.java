package tests;

import java.util.List;

import model.DAO.DAO;
import model.DAO.UserDAO;
import model.classes.Person;
import model.classes.Role;
import model.classes.User;

/**
 * The test class for the UserDAO class
 * 
 * @author Clement AZIBEIRO
 * @version 1.0
 */
public class TestsUserDAO {

	public static void main(String[] args) {
		DAO<User> user_dao = new UserDAO();
		int last_idSupervisor = 0,
			last_idOperator = 0;
		
		Role oneRole = new Role(1, "GOD");
		Role twoRole = new Role(2, "TEST");
		
		Person onePerson = new Person(1, "Clement", "AZIBEIRO");
		User supervisor = new User(onePerson, "c.azibeiro", "password", oneRole, null);
		User operator = new User (2, "Hugo", "MERCIER", "h.mercier", "password", twoRole, supervisor);
		
		// test of the method create
		user_dao.create(operator);
		user_dao.create(supervisor);
		
		// test of the method recupAll
		List<User> list_user;
		list_user = user_dao.recupAll();
		
		for (User user : list_user) {
			System.out.println("ALL WITH CREATED " + user.toString());
			
		}
		
		last_idOperator = list_user.get(list_user.size() - 2).getIdPerson();
		last_idSupervisor = list_user.get(list_user.size() - 1).getIdPerson();
		System.out.println(last_idOperator + " " + last_idSupervisor);
		
		// test of the method read
		System.out.println("\nREAD " + user_dao.read(Integer.toString(last_idSupervisor)).toString());
		System.out.println("READ " + user_dao.read(Integer.toString(last_idOperator)).toString());
		
		supervisor.setIsConnected(true);
		supervisor.setIdPerson(last_idSupervisor);
		
		operator.setIsConnected(true);
		operator.setIdPerson(last_idOperator);
		
		// test of the method update
		user_dao.update(supervisor);
		user_dao.update(operator);
		
		System.out.println("\nUPDATE " + user_dao.read(Integer.toString(last_idSupervisor)).toString());
		System.out.println("UPDATE " + user_dao.read(Integer.toString(last_idOperator)).toString());
		
		// test of the method delete
		user_dao.delete(operator);
		user_dao.delete(supervisor);
	}

}
