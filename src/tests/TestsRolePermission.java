package tests;

import model.classes.Permission;
import model.classes.Role;
import model.classes.RolePermission;

/**
 * The test class for the RolePermission class
 * 
 * @author Clement AZIBEIRO
 * @version 1.0
 */
public class TestsRolePermission {

	public static void main(String[] args) {
		Role oneRole = new Role(1, "GOD");
		Permission onePermission = new Permission(1, "READ");
		
		RolePermission oneRolePermission = new RolePermission(oneRole, onePermission);
		
		System.out.println(oneRolePermission.getOne_role().toString() + " " + oneRolePermission.getOne_permission().toString());
		System.out.println(oneRolePermission.toString());

	}

}
