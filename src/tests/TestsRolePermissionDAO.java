package tests;

import java.util.List;

import model.DAO.DAO;
import model.DAO.RolePermissionDAO;
import model.classes.Permission;
import model.classes.Role;
import model.classes.RolePermission;

/**
 * The test class for the RolePermissionDAO class
 * 
 * @author Clement AZIBEIRO
 * @version 1.0
 */
public class TestsRolePermissionDAO {

	public static void main(String[] args) {
		DAO<RolePermission> role_permission_dao = new RolePermissionDAO();
		int last_idRole_permission = 0;
		
		Role oneRole = new Role(2, "test_role");
		Permission onePermission = new Permission(1, "test_permission");
		
		RolePermission oneRolePermission = new RolePermission(oneRole, onePermission);
		
		// test of the method create
		role_permission_dao.create(oneRolePermission);
		
		// test of the method recupAll
		List<RolePermission> list_RolePermission;
		list_RolePermission = role_permission_dao.recupAll();

		for (RolePermission role_permission : list_RolePermission) {
			System.out.println(role_permission.getOne_role().toString() +" "+role_permission.getOne_permission().toString());
		}
		
		last_idRole_permission = list_RolePermission.get(list_RolePermission.size() - 1).getOne_role().getIdRole();
		
		// test of the method read
		System.out.println(role_permission_dao.read(Integer.toString(last_idRole_permission)).toString());
		
		// test of the method update
		onePermission.setName("This is a name Permission");
		onePermission.setIdPermission(last_idRole_permission);
		
		oneRolePermission.setOne_permission(onePermission);
		
		role_permission_dao.update(oneRolePermission);
		System.out.println(role_permission_dao.read(Integer.toString(last_idRole_permission)).toString());
		
		// test of the method delete
		role_permission_dao.delete(oneRolePermission);
	}
}
