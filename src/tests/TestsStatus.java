package tests;

import model.classes.Status;

/**
 * The test class for the Status class
 * 
 * @author Clement AZIBEIRO
 * @version 1.0
 */
public class TestsStatus {

	public static void main(String[] args) {
		Status oneStatus = new Status(1, "status_test");
		
		System.out.println(oneStatus.getIdStatus() + " " + oneStatus.getDescription());
		System.out.println(oneStatus.toString());
	}
}
