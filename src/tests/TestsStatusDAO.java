package tests;

import java.util.List;

import model.DAO.DAO;
import model.DAO.StatusDAO;
import model.classes.Status;

/**
 * The test class for the StatusDAO class
 * 
 * @author Clement AZIBEIRO
 * @version 1.0
 */
public class TestsStatusDAO {

	public static void main(String[] args) {
		DAO<Status> status_dao = new StatusDAO();
		int last_status = 0;
		
		Status oneStatus = new Status(2, "This is a status");
		
		// test of the method create
		status_dao.create(oneStatus);
		
		// test of the method recupAll
		List<Status> list_Status;
		list_Status = status_dao.recupAll();

		for (Status status : list_Status) {
			System.out.println(status.getIdStatus()+" "+status.getDescription());
		}
		
		last_status = list_Status.get(list_Status.size() - 1).getIdStatus();
		
		// test of the method read
		System.out.println(status_dao.read((Integer.toString(last_status)).toString()));
		
		// test of the method update
		oneStatus.setDescription("This is a description role");
		oneStatus.setIdStatus(last_status);
		status_dao.update(oneStatus);
		System.out.println(status_dao.read(Integer.toString(last_status)).toString());
		
		// test of the method delete
		status_dao.delete(oneStatus);
	}

}
