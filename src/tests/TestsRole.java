package tests;

import model.classes.Role;

/**
 * The test class for the Role class
 * 
 * @author Clement AZIBEIRO
 * @version 1.0
 */
public class TestsRole {

	public static void main(String[] args) {
		Role oneRole = new Role(1, "one_role");

		System.out.println(oneRole.getIdRole() + " " + oneRole.getDescription());
		System.out.println(oneRole.toString());
	}
}
