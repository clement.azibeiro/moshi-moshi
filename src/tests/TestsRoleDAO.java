package tests;

import java.util.List;

import model.DAO.DAO;
import model.DAO.RoleDAO;
import model.classes.Role;

/**
 * The test class for the RoleDAO class
 * 
 * @author Clement AZIBEIRO
 * @version 1.0
 */
public class TestsRoleDAO {

	public static void main(String[] args) {
		DAO<Role> role_dao = new RoleDAO();
		int last_role = 0;
		
		Role oneRole = new Role(2, "This is a role");
		
		// test of the method create
		role_dao.create(oneRole);
		
		// test of the method recupAll
		List<Role> list_Role;
		list_Role = role_dao.recupAll();

		for (Role role : list_Role) {
			System.out.println(role.getIdRole()+" "+role.getDescription());
		}
		
		last_role = list_Role.get(list_Role.size() - 1).getIdRole();
		
		// test of the method read
		System.out.println(role_dao.read((Integer.toString(last_role)).toString()));
		
		// test of the method update
		oneRole.setDescription("This is a description role");
		oneRole.setIdRole(last_role);
		role_dao.update(oneRole);
		System.out.println(role_dao.read(Integer.toString(last_role)).toString());
		
		// test of the method delete
		role_dao.delete(oneRole);
	}

}
