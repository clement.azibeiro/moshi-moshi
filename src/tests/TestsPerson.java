package tests;

import model.classes.Person;

/**
 * The test class for the Person class
 * 
 * @author Clement AZIBEIRO
 * @version 1.0
 */
public class TestsPerson {

	public static void main(String[] args) {
		Person onePerson = new Person(1, "firstname", "lastname");

		System.out.println(onePerson.getIdPerson() + " " + onePerson.getFirstname() + " " + onePerson.getLastname());
		System.out.println(onePerson.toString());
	}
}
