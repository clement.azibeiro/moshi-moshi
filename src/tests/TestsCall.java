package tests;

import java.sql.Date;
import java.util.Calendar;

import model.classes.Call;
import model.classes.ExternPerson;
import model.classes.Role;
import model.classes.Status;
import model.classes.User;
import type.Direction;

/**
 * The test class for the Call class
 * 
 * @author Clement AZIBEIRO
 * @version 1.0
 */
public class TestsCall {

    public static void main(String[] args) {
    	
    User user = new User(1, "Clement", "AZIBEIRO", "c.azibeiro", "password", new Role(1, "god"), null);
    Status status = new Status(1, "reussi");
    ExternPerson extern_person = new ExternPerson(1, "Hugo", "Mercier", "0612131415");
    	
	Call myCall = new Call(1, extern_person, status, user, new Date(Calendar.getInstance().getTime().getTime()), 120, Direction.In);
	
	System.out.println(myCall.getIdCall()+" "+myCall.getExtern_person().getLastname()+" "+myCall.getOne_status().getDescription()+" "+
			   myCall.getOperator().getLogin()+" "+myCall.getDate()+" "+myCall.getDuration()+" "+myCall.getDirection().toString());
	
	System.out.println(myCall.toString());
    }

}
