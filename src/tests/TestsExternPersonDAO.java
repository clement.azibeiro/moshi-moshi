package tests;

import java.util.List;

import model.DAO.DAO;
import model.DAO.ExternPersonDAO;
import model.classes.ExternPerson;

/**
 * The test class for the TestsExternPersonDAO class
 * 
 * @author Clement AZIBEIRO
 * @version 1.0
 */
public class TestsExternPersonDAO {

	public static void main(String[] args) {
		DAO<ExternPerson> ExternPerson_dao = new ExternPersonDAO();
		int last_idExtern_person = 0;
		
		ExternPerson oneExternPerson = new ExternPerson(2, "firstname", "lastname", "0611234568");
		
		// test of the method create
		ExternPerson_dao.create(oneExternPerson);
		
		// test of the method recupAll
		List<ExternPerson> list_Externperson;
		list_Externperson = ExternPerson_dao.recupAll();

		for (ExternPerson Externperson : list_Externperson) {
			System.out.println(Externperson.getIdPerson()+" "+
								Externperson.getFirstname() + " " + 
								Externperson.getLastname() + " " +
								Externperson.getPhoneNumber());
		}
		
		last_idExtern_person = list_Externperson.get(list_Externperson.size() - 1).getIdPerson();
		
		// test of the method read
		System.out.println(ExternPerson_dao.read(Integer.toString(last_idExtern_person)).toString());
		
		// test of the method update
		oneExternPerson.setPhoneNumber("0711234568");
		oneExternPerson.setIdPerson(last_idExtern_person);
		ExternPerson_dao.update(oneExternPerson);
		System.out.println(ExternPerson_dao.read(Integer.toString(last_idExtern_person)).toString());
		
		
		
		// test of the method delete
		ExternPerson_dao.delete(oneExternPerson);
	}
}
