package tests;

import java.util.List;

import model.DAO.DAO;
import model.DAO.PermissionDAO;
import model.classes.Permission;

/**
 * The test class for the PermissionDAO class
 * 
 * @author Clement AZIBEIRO
 * @version 1.0
 */
public class TestsPermissionDAO {

	public static void main(String[] args) {
		Permission onePermission = new Permission(1, "permission_name");
		DAO<Permission> Permission_dao = new PermissionDAO();
		int last_permission = 0;
		
		// test of the method create
		Permission_dao.create(onePermission);
		
		// test of the method recupAll
		List<Permission> list_Permission;
		list_Permission = Permission_dao.recupAll();

		for (Permission permission : list_Permission) {
			System.out.println(permission.getIdPermission()+" "+
					permission.getName());
		}		
		
		last_permission = list_Permission.get(list_Permission.size() - 1).getIdPermission();
		
		// test of the method read
		System.out.println(Permission_dao.read(Integer.toString(last_permission)).toString());
		
		// test of the method update
		onePermission.setName("Permission1");
		onePermission.setIdPermission(last_permission);
		Permission_dao.update(onePermission);
		System.out.println(Permission_dao.read(Integer.toString(last_permission)).toString());		
		
		// test of the method delete
		Permission_dao.delete(onePermission);
	}

}
