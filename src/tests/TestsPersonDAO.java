package tests;

import java.util.List;

import model.DAO.DAO;
import model.DAO.PersonDAO;
import model.classes.Person;

/**
 * The test class for the PersonDAO class
 * 
 * @author Clement AZIBEIRO
 * @version 1.0
 */
public class TestsPersonDAO {
	public static void main(String[] args) {
		DAO<Person> person_dao = new PersonDAO();
		int last_person = 0;
		
		Person onePerson = new Person(3, "firstname", "lastname");
		
		// test of the method create
		person_dao.create(onePerson);
		
		// test of the method recupAll
		List<Person> list_person;
		list_person = person_dao.recupAll();

		for (Person person : list_person) {
			System.out.println(person.getIdPerson()+" "+person.getFirstname() + " " + person.getLastname());
		}
		
		last_person = list_person.get(list_person.size() - 1).getIdPerson();
		
		// test of the method read
		System.out.println(person_dao.read(Integer.toString(last_person)).toString());
		
		// test of the method update
		onePerson.setFirstname("FirstFirstname");
		onePerson.setIdPerson(last_person);
		person_dao.update(onePerson);
		System.out.println(person_dao.read(Integer.toString(last_person)).toString());
		
		// test of the method delete
		person_dao.delete(onePerson);
		
	}
}
