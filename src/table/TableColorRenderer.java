package table;

import java.awt.Color;
import java.awt.Component;

import javax.swing.JTable;
import javax.swing.table.DefaultTableCellRenderer;
import javax.swing.table.TableCellRenderer;

/**
 * TableColorRenderer class is used to color the status cell in the operator table
 * 
 * @author Clement AZIBEIRO
 * @version 1.0
 */
public class TableColorRenderer implements TableCellRenderer{
	private static final TableCellRenderer RENDERER = new DefaultTableCellRenderer();

	@Override
	public Component getTableCellRendererComponent(JTable table, Object value, boolean isSelected, boolean hasFocus,
			int row, int column) {
		Component c =RENDERER.getTableCellRendererComponent(table, value, isSelected, hasFocus, row, column);
		
		if(column == 5) // status column location
		{
			Object result = table.getModel().getValueAt(row, column);
			Boolean status = Boolean.parseBoolean(result.toString());
			Color color = null;
			if(status)
			{
				color = Color.GREEN;
			}
			else
			{
				color = Color.RED;
			}
			c.setBackground(color);
			c.setForeground(color);
		}
		else
		{
			c.setBackground(Color.WHITE);
			c.setForeground(Color.BLACK);
		}
		return c;
	}
}
